package someHelpingFunctions;

import java.util.LinkedList;

import algebraicExpression.AlgebraicExpression;

public class SomeHelpingFunctions {
	
	public static int getIntLength(int num) {
		return (int) (Math.log10(num) + 1);
	}
	
	public static boolean isDouble(String s) {
		try { 
			// Checking valid Double using parseDouble() method 
			Double.parseDouble(s); 
			return true;
        }  
		catch (NumberFormatException e) {
			return false;
		} 
	}
	
	public static boolean isInt(String s) {
		try { 
			// Checking valid Integer using parseInteger() method 
			Integer.parseInt(s);
			return true;
        }  
		catch (NumberFormatException e) {
			return false;
		} 
	}
	
	public static double roundNum(double num, int roundPlaces) {

		 double scale = Math.pow(10, roundPlaces);

		 return Math.round(num * scale) / scale;

	}
	
	public static int flipByDot(double num, int roundPlaces) {
		// 0.36 -> 36
		
		if (roundNum(num, roundPlaces) % 1 == 0) {
			return (int) num;			
		}
		
		return flipByDot(10 * num, roundPlaces);
	}
	
	public static boolean areAllNumericAE(AlgebraicExpression[] args) {
		return isAllNumericAE(args, 0);
	}
	
	private static boolean isAllNumericAE(AlgebraicExpression[] args, int argIndex) {
		if (argIndex >= args.length) {
			return true;
		}
		if (args[argIndex].isNumericExpression()) {
			return isAllNumericAE(args, argIndex + 1);
		}
		return false;
	}
	
	public static boolean isAllInt(double[] args) {
		return isAllInt(args, 0);
	}
	
	private static boolean isAllInt(double[] args, int argIndex) {
		if (argIndex >= args.length) {
			return true;
		}
		if (args[argIndex] % 1 == 0) {
			return isAllInt(args, argIndex + 1);
		}
		return false;
	}
	
	public static boolean isAllPositive(int[] args) {
		for (int num : args) {
			if (num < 0) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isAllNegative(int[] args) {
		for (int num : args) {
			if (num > 0) {
				return false;
			}
		}
		return true;
	}
	
	public static int[] makeAllPositive(int[] args) {
		for (int i = 0; i < args.length; i++) {
			args[i] = Math.abs(args[i]);
		}
		return args;
	}
	
	public static <E> LinkedList<E> addArrayToLinkedList(LinkedList<E> l, E[] a) {
		for (int i = 0; i < a.length; i++){
			  l.addLast(a[i]);
		}
		
		return l;
	}
	
	public static double charArrayToDouble(char[] array) {
		return Double.valueOf(new String(array));
	}
	
	public static double[] AEArrayToDoubleArray(AlgebraicExpression[] array) {
		double[] newArray = new double[array.length];
		for (int i = 0; i < newArray.length; i++) {
			newArray[i] = array[i].calculate();
		}
		return newArray;
	}
	
	public static Integer[] AEArrayToIntArray(AlgebraicExpression[] array) {
		Integer[] newArray = new Integer[array.length];
		double temp;
		
		for (int i = 0; i < newArray.length; i++) {
			temp = array[i].calculate();
			if (temp % 1 == 0) {
				newArray[i] = (int) temp;
			}
			else {
				return null;
			}
		}
		return newArray;
	}
	
	public static int[] integerArrayToIntArray(Integer[] integerArray) {

		int[] result = new int[integerArray.length];
		for (int i = 0; i < integerArray.length; i++) {
			result[i] = integerArray[i];
		}
		return result;
	}
	
	public static double[] intArrayToDoubleArray(int[] array) {
		double[] newArray = new double[array.length];
		for (int i = 0; i < newArray.length; i++) {
			newArray[i] = (double) array[i];
		}
		return newArray;
	}
	
	public static String[] toStringArray(Object[] array) {
		String[] newArray = new String[array.length];
		for (int i = 0; i < newArray.length; i++) {
			newArray[i] = String.valueOf(array[i]);
		}
		return newArray;
	}
	
	public static double[] stringArrayToDoubleArray(String[] array) {
		double[] newArray = new double[array.length];
		for (int i = 0; i < newArray.length; i++) {
			newArray[i] = Double.valueOf(array[i]);
		}
		return newArray;
	}
	
	public static int[] doubleArrayToIntArray(double[] array) {
		int[] newArray = new int[array.length];
		for (int i = 0; i < newArray.length; i++) {
			newArray[i] = (int) array[i];
		}
		return newArray;
	}

	public static String[] intArrayToStringArray(int[] intArray) {
		String[] strArray = new String[intArray.length];
		for (int i = 0; i < intArray.length; i++) {
			strArray[i] = String.valueOf(intArray[i]);
		}
		return strArray;
	}
	
	public static String[] doubleArrayToStringArray(double[] intArray) {
		String[] strArray = new String[intArray.length];
		for (int i = 0; i < intArray.length; i++) {
			strArray[i] = String.valueOf(intArray[i]);
		}
		return strArray;
	}
	
	public static Integer[] intArrayToIntegerArray(int[] intArray) {
		Integer[] strArray = new Integer[intArray.length];
		for (int i = 0; i < intArray.length; i++) {
			strArray[i] = Integer.valueOf(intArray[i]);
		}
		return strArray;
	}
	
	public static AlgebraicExpression[] toAEArray(Object[] strArray) {
		AlgebraicExpression[] AEArray = new AlgebraicExpression[strArray.length];
		for (int i = 0; i < strArray.length; i++) {
			AEArray[i] = AlgebraicExpression.valueOf(strArray[i]);
		}
		return AEArray;
	}

	public static Integer[] toInts(AlgebraicExpression[] args) {
		Integer[] strArray = new Integer[args.length];
		for (int i = 0; i < args.length; i++) {
			strArray[i] = (int) (double) args[i].calculate();
		}
		return strArray;
	}

}
