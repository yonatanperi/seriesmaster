package algebraicExpression;
import java.util.LinkedList;
import java.util.Hashtable;

import someHelpingFunctions.SomeHelpingFunctions;

import java.util.Arrays;


public class AlgebraicExpression {
	
	// all the nums will be on double format!
	
	// SAE = StringAlgebraicExpression
	private BinNode root;
	
	/**
	 * a^b = Math.pow(a, b)
	 * a#b = log a(b)
	 * _ = -()
	 */	
	private final static char[] OPERATORS = {'+', '-', '*', '/', '^', '#', '_'};
	
	/**
	 * When placing in a value, the placed char will be the AEChar.
	 */
	private char AEChar; // For numeric AE: ' '
	
	private final static Hashtable<Character, Integer> OPERATORS_RANK = new Hashtable<Character, Integer>();
	
	/**
     * Constructs an empty AE.
     */
	public AlgebraicExpression(){}
	
	AlgebraicExpression(BinNode root){
		this(root, true);
	}
	
	AlgebraicExpression(BinNode root, char AEChar) {
		this(root, AEChar, true);
	}
	
	AlgebraicExpression(BinNode root, boolean toSimplify) {
		this(root, getAEChar(toString(root, false)), toSimplify);
	}
	
	AlgebraicExpression(BinNode root, char AEChar, boolean toSimplify) {
		OPERATORS_RANK.put('+', 0);
		OPERATORS_RANK.put('-', 0);
		OPERATORS_RANK.put('*', 1);
		OPERATORS_RANK.put('/', 2);
		OPERATORS_RANK.put('^', 3);
		OPERATORS_RANK.put('#', 4);
		OPERATORS_RANK.put('_', 5);
		this.setAlgebraicExpression(root, AEChar, toSimplify);
	}
	
	/**
	 * A copy constructor.
	 * @param ae is the AE which will be copied.
	 */
	public AlgebraicExpression(AlgebraicExpression ae){
		this(getBinNodeCopy(ae.getRoot()), ae.getAEChar(), false);
	}
	
	AlgebraicExpression(String ae, char AEChar, boolean toSimplify){
		this(toAlgebraicExpressionTree(fixString(ae)), AEChar, toSimplify);
	}	

	AlgebraicExpression(String ae, boolean toSimplify){
		this(ae, getAEChar(ae), toSimplify);
	}
	
	/**
	 * Constructs new AE from String
	 * @param ae is the String of the AE
	 * @param AEChar is the placed char in the getValue function.
	 */
	public AlgebraicExpression(String ae, char AEChar) {
		this(ae, AEChar, true);
	}
	
	/**
	 * Constructs new AE from String
	 * @param ae is the String of the AE
	 * The AEChar is the first char in the AE tree.
	 */
	public AlgebraicExpression(String ae) {
		this(ae, getAEChar(ae));
	}
	
	void setAlgebraicExpression(BinNode root, char AEChar, boolean toSimplify) {
		this.setAEChar(AEChar);
		this.setRoot(root);
		if (toSimplify) {
			this.simplify();
		}
	}
	
	void setAlgebraicExpression(AlgebraicExpression ae, boolean toSimplify) {
		this.setAlgebraicExpression(ae.getRoot(), ae.getAEChar(), toSimplify);
	}
	
	private void setRoot(BinNode root) {
		this.root = root;
	}
	
	private static char getAEChar(String SAE) {
		return getAEChar(SAE, 0);
	}

	private static char getAEChar(String SAE, int charIndex) {
		if (charIndex == SAE.length()) {
			return ' ';
		}
		char currentChar = SAE.charAt(charIndex);
		if (Character.isLetter(currentChar)) {
			return currentChar;			
		}
		return getAEChar(SAE, charIndex + 1);
	}
	
	void setAEChar(char AEChar) {
		this.AEChar = AEChar;
	}
	
	private static String fixString(String SAE) {
		if (SAE.isEmpty()) {
			return "";
		}
		
		SAE = SAE.replace("-(", "_(");

		String newSAE = "";
		for (int i = 0; i < SAE.length() - 1; i++) {
			if (SAE.charAt(i) != ' ') {
				newSAE += SAE.charAt(i);
				if ((!isOperator(SAE.charAt(i)) && 
						SAE.charAt(i) != '(') && (Character.isLetter(SAE.charAt(i + 1)) || SAE.charAt(i + 1) == '(')){
					newSAE += '*';
				}
			}
		}
		return (newSAE + SAE.charAt(SAE.length() - 1));
	}
	
	public AlgebraicExpression simplify() {
		this.setRoot(SimplifyAlgebraicExpression.simplify(this).getRoot());
		return this;
	}
	
	/**
	 * Merge 2 AEs under an operator.
	 * @return the merged AE;
	 */
	public static AlgebraicExpression mergeAlgebraicExpressions(AlgebraicExpression ae1, AlgebraicExpression ae2, char operator) {		
		return mergeAlgebraicExpressions(ae1, ae2, operator, true);
	}
	
	static AlgebraicExpression mergeAlgebraicExpressions(AlgebraicExpression ae1, AlgebraicExpression ae2, char operator, boolean toSimplify) {
		
		if (ae1 == null || ae2 == null) {
			return null;
		}

		switch (operator) {
			case '-':
				operator = '_';
				break;				
			case '*':
				if (ae1.toString().equals("1")) {
					return ae2;
				}
			case '/':
				if (ae2.toString().equals("1")) {
					return ae1;
				}
		}
		
		AlgebraicExpression ae3 = new AlgebraicExpression(); // Returned ae
		
		if (ae1.isEmpty()) {
			ae3.setAlgebraicExpression(ae2, toSimplify);
		}
		else if (ae2.isEmpty()) {
			ae3.setAlgebraicExpression(ae1, toSimplify);
		}
		else {
			
			ae3.setAEChar(ae1.getAEChar());
			BinNode newRoot;
			
			newRoot = new BinNode(operator);
			String strAE2 = ae2.toString();
			if (operator == '+' && strAE2.charAt(0) == '-') { // deals with a+-b
				ae2 = new AlgebraicExpression(ae2.toString().substring(1), ae2.getAEChar(), false);		
				if (strAE2.charAt(1) == '(') {
					newRoot.data = ('_');
				}
				else {
					newRoot.data = ('-');
				}
			}
			
			newRoot.left = ae1.getRoot();
			newRoot.right = ae2.getRoot();
			
			if (operator == '*') {
				SimplifyAlgebraicExpression.fixMultiplation(newRoot);
			}
			
			ae3.setRoot(newRoot);	
		}		
		
		if (toSimplify) {
			ae3.simplify();
		}
		
		return ae3;
	}
	
	/**
	 * Merge an array of AEs.
	 * @param aes
	 * @param operator
	 * @return the merged AE;
	 */
	public static AlgebraicExpression mergeAlgebraicExpressions(AlgebraicExpression[] aes, char operator) {
		return mergeAlgebraicExpressions(aes, operator, true);
	}
	static AlgebraicExpression mergeAlgebraicExpressions(AlgebraicExpression[] aes, char operator, boolean toSimplify) {
		if (aes.length == 0) {			
			return new AlgebraicExpression();
		}
		if (aes.length == 1) {			
			return new AlgebraicExpression(aes[0].root, aes[0].getAEChar(), toSimplify);
		}
		if (aes[0].isEmpty()) {			
			return mergeAlgebraicExpressions(Arrays.copyOfRange(aes, 1, aes.length), operator, toSimplify);
		}
		aes[1] = mergeAlgebraicExpressions(aes[0], aes[1], operator, toSimplify);
		aes = Arrays.copyOfRange(aes, 1, aes.length);	
		return mergeAlgebraicExpressions(aes, operator, toSimplify);
	}
	
	BinNode getRoot() {
		return this.root;
	}
	
	char getAEChar() {
		return this.AEChar;
	}
	
	static BinNode getBinNodeCopy(BinNode node) {
		return getCopyOfBinNode(node, new BinNode(node));
	}
	
	private static BinNode getCopyOfBinNode(BinNode orNode, BinNode copyNode) {
		if (orNode == null) {
			return null;
		}
		copyNode = new BinNode(orNode);
		copyNode.left = getCopyOfBinNode(orNode.left, copyNode.left);
		copyNode.right = getCopyOfBinNode(orNode.right, copyNode.right);
		return copyNode;
	}
	
	public String toString() {		
		return toString(true);
	}
	
	String toString(boolean addMultiplicationSigns) {
		if (this.isEmpty()) {
			return "";
		}
		return toString(this.getRoot(), addMultiplicationSigns);
	}
	
	static String toString(BinNode node, boolean addMultiplicationSigns) {
		// left - right - root
		
		String[] sides = {"", ""}; // left, right
		
		if (node.left != null) {
			if (SomeHelpingFunctions.isDouble(String.valueOf(node.data))) {
				return (getNumfromTree(node));
			}
			sides[0] = toString(node.left, addMultiplicationSigns);
			sides[1] = toString(node.right, addMultiplicationSigns);
			
			if (isOperator(node.data)) {
				
				int flag = 0;
				/*
				 * 0: side operator side
				 * 1: (side) operator side
				 * 2: side operator (side)
				 * 3: (side) operator (side)
				 */	
				
				if ((compareOperators(node.data, node.left.data) == 1 && !isNegNum(node.left)) ||
						(node.data == '^' && node.left.data == '^')) {
					flag = 1;
				}
				if ((compareOperators(node.data, node.right.data) == 1 && !isNegNum(node.right)) ||
						(node.data == '^' && node.right.data == '^') ||
						node.right.data == '#') {
					if (flag == 1) {
						flag = 3;
					}
					else {
						flag = 2;
					}
				}
				
				if (isNegNum(node)) { // neg num check
					sides[0] = "";
				}
				
				String operator;
				if ((!addMultiplicationSigns) && node.data == '*' && (Character.isLetter(node.right.data) || node.right.data == '(')) {
					// a*b -> ab, a*( -> a(
					operator = "";
				}
				else {
					operator = String.valueOf(node.data);
					if (operator.equals("_")) {
						operator = "-";
					}
				}
				
				switch (flag) {
					case 0:
						break;
					case 1:
						return ('(' + sides[0] + ')' + operator + sides[1]);
					case 2:
						return (sides[0] + operator + '(' + sides[1] + ')');
					case 3:
						return ('(' + sides[0] + ')' + operator + '(' + sides[1] + ')');
				
				}			
			}
		}
		
		return (sides[0] + node.data + sides[1]);		
	}
	
	static boolean isNegNum(BinNode node) {
		// node is the suspect '-' sign
		/*
		 * the format of neg num:
		 *            -
		 *           / \
		 *          0   a
		 *         /\   /\
		 *        /  \ /  \
		 *      null null null
		 */
		if (node == null) {
			return false;
		}
		if (isNeg(node.data)) {
			return node.left.data == '0' && node.left.left == null;
		}
		return false;
	}
	
	private static int compareOperators(char o1, char o2) {
		
		/*
		 * -1: o1 or o2 is not an operator
		 * 0: o1 == o2
		 * 1: o1 > o2
		 * 2: o1 < o2
		 */
		
		if (isOperator(o1) && isOperator(o2)) {
			
			int i1 = OPERATORS_RANK.get(o1);
			int i2 = OPERATORS_RANK.get(o2);

			if (i1 == i2 && !(o1 == '/' && o2 == '/')) {
				return 0;
			}
			else if (i1 < i2) {
				return 2;
			}
			else {
				return 1;
			}
		}
		
		return -1;
	}
	
	/**
	 * Place a certain value in the AEChar.
	 * 
	 * @param n is the certain value.
	 * @return the AE after the placing of @param n.
	 */
	public AlgebraicExpression getValue(double n) {
		BinNode placedAE = this.getValue(String.valueOf(n), getBinNodeCopy(this.getRoot()));
		if (isNumericExpression(placedAE)) {
			return AlgebraicExpression.valueOf(this.getDoubleValue(0, placedAE), ' ', false);
		}
		return new AlgebraicExpression(placedAE, this.getAEChar(), true);
	}
	
	private BinNode getValue(String n, BinNode node) {
		if (node == null) {
			return null;
		}
		if (node.data == this.getAEChar()) {
			node = setNumInTree(n);
		}
		node.left = this.getValue(n, node.left);
		node.right = this.getValue(n, node.right);
		return node;
	}
	
	Double getDoubleValue(double n) {
		if (!this.isEmpty()) {
			return this.getDoubleValue(n, this.getRoot());
		}
		return null;
	}
	
	private double getDoubleValue(double n, BinNode node) {
		// in order travel
		
		if (isOperator(node.data)) {
			double left, right;
			left = getDoubleValue(n, node.left);
			right = getDoubleValue(n, node.right);
			return executeElementaryArithmetic(left, right, node.data);
		}
		
		// it means node is AEChar or a num.
		
		if (node.data == this.AEChar) { // AEChar
			return n;
		}
		return Double.parseDouble(getNumfromTree(node)); // just a num
	}
	
	private static double executeElementaryArithmetic(double n1, double n2, char operator) {
		switch(operator){
    		case '+':
    			return n1 + n2;
    		case '_':
    		case '-':
    			return n1 - n2;
    		case '*':
    			return n1 * n2;
    		case '/':
    			return n1 / n2;
    		case '^':
    			return Math.pow(n1, n2);
    		case '#':
    			return Math.log(n2) / Math.log(n1);
    		default:
    			return 0;
		}
	}
	
	public static char getDerivativeOperator(char operator) {
		switch(operator){
			case '*':
				return '+';
			case '/':
				return '-';
			case '^':
				return '*';
			default:
				return operator;
		}
	}
	
	public static char getCounterOperator(char operator) {
		switch(operator){
    		case '+':
    			return '-';
    		case '_':
    		case '-':
    			return '+';
    		case '*':
    			return '/';
    		case '/':
    			return '*';
    		case '^':
    			return '#';
    		case '#':
    			return '^';    		
		}
		return ' ';
	}
	
	/**
	 * AE -> -(AE)
	 */
	public void makeNeg() {
		BinNode newRoot = new BinNode('_');
		newRoot.left = new BinNode('0');
		newRoot.right = root;
		this.setRoot(newRoot);
	}
	
	static boolean isNeg(char c) {
		return c == '-' || c == '_';
	}
	
	public static char[] getAllOperators() {		
		return OPERATORS;
	}

	static boolean isOperator(char c) {
        for (char operator : getAllOperators()) {
        	if (c == operator) {
        		return true;
        	}
        }
        return false; 
    } 
	
	private static int findMidOperator(String SAE) {
		
		@SuppressWarnings("unchecked")
		LinkedList<Integer>[] operatorsList = (LinkedList<Integer>[]) new LinkedList[5];
		for (int i = 0; i < operatorsList.length; i++) { // fills the operatorsStaks.
			operatorsList[i] = new LinkedList<Integer>();
		}
		/* operatorsList contains the location of the operators in the SAE.
		 * operatorsList = [ListOf(+, -), ListOf(*, /), ListOf(^), , ListOf(#)]
		 */
		int parenthesisFlag = 0; // parenthesis doesn't count!
		
		for (int i = 0; i < SAE.length(); i++) { // pushes into the operatorsStaks.
			char currentChar = SAE.charAt(i);
			
			if (currentChar == '(') {
				parenthesisFlag += 1;
				continue;
			}
			else if (currentChar == ')') {
				parenthesisFlag -= 1;
				continue;
			}
			
			// Doesn't count negative nums
			if (i == 0 && isNeg(currentChar)) {
				continue;
			}
			if (i != 0) {
				if (isOperator(SAE.charAt(i - 1))) {
					continue;
				}					
			}

			
			if (isOperator(currentChar) && parenthesisFlag == 0) {
				switch (currentChar){
					case '+':
					case '-':
						operatorsList[0].addLast(i);
						break;
					case '/':					
						operatorsList[1].addLast(i);
						break;
					case '*':
						operatorsList[2].addLast(i);
						break;
					case '^':
						operatorsList[3].addLast(i);
						break;
					default: // #
						operatorsList[4].addLast(i);
						break;
				}
			}
			
		}
		
		for (int index = 0; index < operatorsList.length; index++) {
			if (operatorsList[index].size() != 0) {
				if (index == 1) {
					return operatorsList[index].getLast();
				}
				else if (index == 3 && isNeg(SAE.charAt(0))) { // -(a)^n != (-a)^n
					continue;
				}
				return operatorsList[index].get(operatorsList[index].size() / 2);
			}
		}
		return -1;
	}

	private static BinNode toAlgebraicExpressionTree(String SAE) {
		
		// returns the root of the tree
		
		if (SAE.equals("")) {
			return null;
		}
		
		if (SAE.length() == 1) {
			return new BinNode(SAE.charAt(0));
		}
		
		int midOperatorIndex = findMidOperator(SAE);
		if (midOperatorIndex == -1) { // if num.length > 1 or negative num or (Expression)
			if (SAE.charAt(0) == '(') {
				// deletes the parenthesis in case of: (Expression)
				// and deals with '((...))'
				return toAlgebraicExpressionTree(SAE.substring(1, SAE.length() - 1));
			}
			return setNumInTree(SAE);
		}
		
		BinNode rootNode = new BinNode(SAE.charAt(midOperatorIndex));
		rootNode.left = toAlgebraicExpressionTree(SAE.substring(0, midOperatorIndex));
		rootNode.right = toAlgebraicExpressionTree(SAE.substring(midOperatorIndex + 1, SAE.length()));
		
		if (rootNode.data == '*') {
			if (rootNode.left.data == '/' && SAE.charAt(midOperatorIndex) - 1 == ')') {
				// (a/b)*c -> c*a/b
				rootNode = fixMultiplationDividing(rootNode);
			}
		}		
		return rootNode;
	}

	private static BinNode fixMultiplationDividing(BinNode node) {
		// (a/b)*c -> c*a/b
		
		node.data = '/';
		node.left.data = '*';
		BinNode tempPointer = node.left.left; // ->a
		node.left.left = node.right;
		node.right = node.left.right;
		node.left.right = tempPointer;
		
		return node;
	}
	
	static BinNode setNumInTree(String num) {
		
		if (num.length() > 2) {
			if (num.substring(num.length() - 2).equals(".0")) { // a.0 -> a
				num = num.substring(0, num.length() - 2);
			}
		}		
		if (isNeg(num.charAt(0))) { // if num is negative
			return setNegativeNumInTree(num);
		}
		if (num.charAt(0) == 'E') { // E means 10^
			return toAlgebraicExpressionTree("10^" + num.substring(1));
		}
		return setLongNumInTree(num);
	}
	
	private static BinNode setNegativeNumInTree(String num) {
		BinNode rootNode;
		rootNode = new BinNode(num.charAt(0));
		rootNode.left = new BinNode('0');
		rootNode.right = new AlgebraicExpression(num.substring(1, num.length()), false).getRoot();
		return rootNode;
	}
	
	private static BinNode setLongNumInTree(String num) {
		
		if (num.length() == 1) {
			return new BinNode(num.charAt(0));
		}
		
		char currentChar = num.charAt(num.length() - 1);
		
		if (Character.isDigit(currentChar) || currentChar == '.') { // Skips the non-digits
			BinNode node = new BinNode(currentChar);
			node.left = setLongNumInTree(num.substring(0, num.length() - 1));;
			return node;
		}
		
		return setLongNumInTree(num.substring(0, num.length() - 1));
	}

	
	static String getNumfromTree(BinNode node) {
				
		String nodeCharString = Character.toString(node.data);
		
		if (node.left == null || node.right != null) {
			return nodeCharString;
		}
		
		return getNumfromTree(node.left) + nodeCharString;
	}	
	
	public boolean isNumericExpression() {
		return isNumericExpression(this.root);
	}
	
	static boolean isNumericExpression(BinNode node) {
		if (node == null) {
			return true;
		}
		
		boolean left, right;
		
		if (Character.isLetter(node.data) && node.data != 'E') {
			return false;
		}
		left = isNumericExpression(node.left);
		right = isNumericExpression(node.right);
		return left && right;
	}
	
	public Double calculate() {
		if (this.isNumericExpression()) {
			return this.getDoubleValue(0);
		}
		return null;
	}
	
	public boolean isEmpty() {		
		return this.getRoot() == null;
	}
	
	static AlgebraicExpression get1AE() {
		return new AlgebraicExpression("1", ' ', false);
	}
	
	/**
	 * 
	 * @return the AE value of an object.
	 */
	public static AlgebraicExpression valueOf(Object obj) {
		return new AlgebraicExpression(String.valueOf(obj));
	}
		
	static AlgebraicExpression valueOf(Object obj, char AEChar) {
		return new AlgebraicExpression(String.valueOf(obj), AEChar);
	}
	
	static AlgebraicExpression valueOf(Object obj, boolean toSimplify) {
		return new AlgebraicExpression(String.valueOf(obj), toSimplify);
	}
	
	static AlgebraicExpression valueOf(Object obj, char AEChar, boolean toSimplify) {
		return new AlgebraicExpression(String.valueOf(obj), AEChar, toSimplify);
	}
	
	public static boolean equals(AlgebraicExpression[] aes1, AlgebraicExpression[] aes2) {
		if (aes1 == aes2) {
			return true;
		}
        if (aes1 == null || aes2 == null) {
        	return false;
        }

        int length = aes1.length;
        if (aes2.length != length) {
        	return false;
        }

        for (int i = 0; i < length; i++) {
            if (!aes1[i].equals(aes2[i])) {
            	return false;
            }
        }

        return true;
	}
	
	public boolean equals(AlgebraicExpression ae, boolean toSimplify) {
		if (toSimplify) {
			return this.equals(ae);
		}
		
		if (ae == null) {
			return false;
		}
		
		return this.toString().equals(ae.toString());
	}
	
	public int compareTo(AlgebraicExpression ae) {		
		/*
		 * -1: Couldn't compare
		 * 0: this == ae
		 * 1: this > ae
		 * 2: this < ae
		 */
		
		AlgebraicExpression difference = mergeAlgebraicExpressions(this, ae, '-');
		if (difference.isNumericExpression() && !difference.isEmpty()) {
			double doubleDifference = difference.calculate();			
			if (doubleDifference > 0) {
				return 1;				
			}
			if (doubleDifference < 0) {
				return 2;				
			}
			return 0;
		}
		return -1;
	}
	
	public boolean equals(Object o) {
		
		if (o == null) {
			return false;
		}
		
		return this.compareTo((AlgebraicExpression) o) == 0;
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}

}
