package algebraicExpression;

import java.util.Hashtable;

class Fraction extends Arg {
	
	// numerator/denominator
	
	Fraction(){
		super();
	}
	
	Fraction(AlgebraicExpression ae){
		this.setFraction(ae);
	}
	
	Fraction(Fraction f) {
		this.setFraction(f);
	}
	
	Fraction(Arg arg){		
		this.setArg(arg);
	}
	
	Fraction(AlgebraicExpression numerator, AlgebraicExpression denominator, char AEChar){
		this.setFraction(numerator, denominator, AEChar);
	}
	
	protected void setArg(AlgebraicExpression argType, AlgebraicExpression coefficient, char AEChar) {
		this.setArg(new Arg(argType, coefficient, AEChar));
	}
	
	protected void setArg(Arg arg) {		
		this.setFraction(arg.getAlgebraicExpression());
	}
	
	private void setFraction(Fraction f) {
		this.setFraction(f.getNumerator(), f.getDenominator(), f.getAEChar());
	}
	
	private void setFraction(AlgebraicExpression argAE) {
		this.setFraction(AlgebraicExpression.get1AE(), AlgebraicExpression.get1AE(), argAE.getAEChar());
		Hashtable<Character, AlgebraicExpression> aeDividedByCharsHash = SimplifyAlgebraicExpression.getAEDividedByCharsHash(argAE);
		
		AlgebraicExpression currentAE;
		
		for (char c : aeDividedByCharsHash.keySet()) {
			currentAE = aeDividedByCharsHash.get(c);
			if (isFraction(currentAE)) {
				currentAE = new AlgebraicExpression(new AlgebraicExpression(SimplifyAlgebraicExpression._ToMinus1Multiplation(currentAE.getRoot()), currentAE.getAEChar(), false).toString(), currentAE.getAEChar(), false); // To make sure the root is '/'
				this.setNumerator(AlgebraicExpression.mergeAlgebraicExpressions(this.getNumerator(), new AlgebraicExpression(currentAE.getRoot().left, this.getAEChar(), false), '*', false), false);
				this.setDenominator(AlgebraicExpression.mergeAlgebraicExpressions(this.getDenominator(), new AlgebraicExpression(currentAE.getRoot().right, this.getAEChar(), false), '*', false), false);			}
			else {
				this.setNumerator(AlgebraicExpression.mergeAlgebraicExpressions(this.getNumerator(), currentAE, '*', false), false);
			}
		}
		
		if (!this.getNumerator().isNumericExpression()) {
			this.setNumerator(this.getNumerator().simplify(), false);
		}
		this.setDenominator(this.getDenominator().simplify()); // reduct
	}

	private void setFraction(AlgebraicExpression numerator, AlgebraicExpression denominator, char AEChar) {		
		if (denominator.toString().equals("0")) { // a/0 -> null				
			return;
		}
		else if (numerator.toString().equals("0")) {		
			denominator = AlgebraicExpression.get1AE();
		}
		super.setArg(denominator, numerator, AEChar);
	}
	
	static boolean isFraction(AlgebraicExpression ae) {
		return getFirstDivisionSign(ae) != null;
	}
	
	static boolean isFraction(Arg arg) {
		if (arg.isEmpty()) {
			return false;
		}
		return getFirstDivisionSign(arg.getAlgebraicExpression()) != null;
	}
	
	static BinNode getFirstDivisionSign(AlgebraicExpression ae) {
		if (ae.isEmpty()) {
			return null;
		}
		return getFirstDivisionSign(ae.getRoot());
	}
	
	private static BinNode getFirstDivisionSign(BinNode node) {
		// left - root - right
		
		if (node == null) {
			return null;
		}
		
		switch(node.data) {
			case '/':
				return node;
			case '_':
			case '*':
				BinNode left, right;
				
				left = getFirstDivisionSign(node.left);		
				if (left == null) {
					right = getFirstDivisionSign(node.right);
					if (right == null) {
						return null;
					}
					else {
						return right;
					}
				}
				else {
					return left;
				}			
		}
		return null;		
	}	
	
	protected Arg multiplyWithArg(Arg arg) {
		Fraction finalFraction = new Fraction(this);
		if (isFraction(arg)) {
			Fraction fractionArg = new Fraction(arg);
			finalFraction.setNumerator(AlgebraicExpression.mergeAlgebraicExpressions(this.getNumerator(), fractionArg.getNumerator(), '*'), false);
			finalFraction.setDenominator(AlgebraicExpression.mergeAlgebraicExpressions(this.getDenominator(), fractionArg.getDenominator(), '*'), false);
		}
		else {
			finalFraction.setNumerator(AlgebraicExpression.mergeAlgebraicExpressions(this.getNumerator(), arg.getAlgebraicExpression(), '*'), false);
		}
		return finalFraction.reduct();
	}
	
	protected Arg addArg(Arg arg) {
		
		Fraction fractionArg = new Fraction(arg);
		Fraction finalFraction;
		
		if (this.getDenominator().isNumericExpression() && fractionArg.getDenominator().isNumericExpression()) {
			// It should be an int after the reduction.
			int thisDenominator = (int) (double) this.getDenominator().calculate();
			int argDenominator = (int) (double) fractionArg.getDenominator().calculate();
			int commonDenominator = getCommonDenominator(thisDenominator, argDenominator);
			finalFraction = new Fraction(AlgebraicExpression.mergeAlgebraicExpressions(
					AlgebraicExpression.mergeAlgebraicExpressions(AlgebraicExpression.valueOf(commonDenominator / thisDenominator, ' ', false), this.getNumerator(), '*'),
					AlgebraicExpression.mergeAlgebraicExpressions(AlgebraicExpression.valueOf(commonDenominator / argDenominator, ' ', false), fractionArg.getNumerator(), '*'), '+'),
					AlgebraicExpression.valueOf(commonDenominator, ' ', false), this.getAEChar());
		}
		else {
			finalFraction = new Fraction(AlgebraicExpression.mergeAlgebraicExpressions(
					AlgebraicExpression.mergeAlgebraicExpressions(fractionArg.getDenominator(), this.getNumerator(), '*'),
					AlgebraicExpression.mergeAlgebraicExpressions(this.getDenominator(), fractionArg.getNumerator(), '*'), '+'),
					AlgebraicExpression.mergeAlgebraicExpressions(this.getDenominator(), fractionArg.getDenominator(), '*'),
					this.getAEChar());

		}
		return finalFraction;
	}
	
	private static int getCommonDenominator(int d1, int d2) {
		int temp;
		
		// d1 is bigger!
		if (d1 < d2) {
			// Switch!
			temp = d1;
			d1 = d2;
			d2 = temp;
		}
		
		temp = d1;
		while (temp % d2 != 0) {
			temp += d1;
		}
		
		return temp;
	}
	
	Fraction expendBy(AlgebraicExpression by) {
		this.setNumerator(AlgebraicExpression.mergeAlgebraicExpressions(this.getNumerator(), by, '*'), false);
		this.setDenominator(AlgebraicExpression.mergeAlgebraicExpressions(this.getDenominator(), by, '*'), false);
		return this;
	}

	private Fraction reduct() {
		
		if (this.getDenominator().toString().equals("-1")) {
			this.getNumerator().makeNeg();
			this.getNumerator().simplify();
			this.setDenominator(AlgebraicExpression.get1AE(), false);
			return this;
		}
		if (isFraction(this.getDenominator()) || isFraction(this.getNumerator())) {
			this.setFraction((Fraction) Arg.mergeArgs(new Arg(this.getNumerator()), new Arg(this.getDenominator()), '/'));
			return reduct();
		}
		
		// Activates noa on the Numerator & Denominator separately
		AlgebraicExpression[] noaNumerator, noaDenominator;
		AlgebraicExpression[][] fractionNoa;
		
		noaNumerator = SimplifyAlgebraicExpression.noa(this.getNumerator());
		noaDenominator = SimplifyAlgebraicExpression.noa(this.getDenominator());
		
		// Activates noa on the noas from the Numerator & Denominator
		for (int i = 0; i < 2; i++) {
			fractionNoa = SimplifyAlgebraicExpression.noa(
					new AlgebraicExpression[] {noaNumerator[i], noaDenominator[i]}, this.getAEChar());
			
			// The actual reduction
			noaNumerator[i] = fractionNoa[1][0];
			noaDenominator[i] = fractionNoa[1][1]; // I don't care about his noas
		}
		
		// Sets the new reduced Numerator & Denominator
		this.setNumerator(AlgebraicExpression.mergeAlgebraicExpressions(noaNumerator, '*'), false);
		this.setDenominator(AlgebraicExpression.mergeAlgebraicExpressions(noaDenominator, '*'), false);
		
		return this;
	}
	
	AlgebraicExpression getNumerator() {
		return this.coefficient;
	}
	
	AlgebraicExpression getDenominator() {
		return this.argType;
	}
	
	void setNumerator(AlgebraicExpression numerator) {
		this.setNumerator(numerator, true);
	}
	
	void setDenominator(AlgebraicExpression denominator) {
		this.setDenominator(denominator, true);
	}
	
	private void setNumerator(AlgebraicExpression numerator, boolean toNoa) {
		this.setCoefficient(numerator);
		if (toNoa) {
			this.reduct();
		}		
	}
	
	private void setDenominator(AlgebraicExpression denominator, boolean toNoa) {
		this.setArgType(denominator);
		if (toNoa) {
			this.reduct();
		}
	}
	
	protected void setArgType(AlgebraicExpression argType) {
		BinNode rootNode = argType.getRoot();
		if (rootNode.data == '/') {
			if (rootNode.left.data == '1' && rootNode.left.left == null) {
				// 1/real argType -> real argType
				argType = new AlgebraicExpression(rootNode.right, argType.getAEChar(), false);
			}
		}
		this.argType = argType;
	}
	
	AlgebraicExpression getArgType() {
		if (this.getDenominator() == null) {
			return null;
		}
		
		if (this.getDenominator().toString().equals("1")) {
			return this.getDenominator();
		}
		BinNode root = new BinNode('/');
		root.left = new BinNode('1');
		root.right = this.getDenominator().getRoot();
		return new AlgebraicExpression(root, this.getAEChar(), false);
	}
	
	AlgebraicExpression getCoefficient() {
		return this.getNumerator();
	}
	
	AlgebraicExpression getAlgebraicExpression() {

		if (this.isEmpty()) {
			return new AlgebraicExpression();
		}
		if (this.getDenominator().toString().equals("1")) { // a/1 --> a
			return getNumerator();
		}
		if (this.getNumerator().toString().equals("0")) { // 0/a --> 0
			return this.getNumerator();
		}
		
		return AlgebraicExpression.mergeAlgebraicExpressions(this.getNumerator(), this.getDenominator(), '/', false);
	}
	
	/** just flips the Fraction
	 * 3     5
	 * - --> -
	 * 5     3
	 */
	Fraction flip() {		
		AlgebraicExpression oldNumerator = new AlgebraicExpression(this.getNumerator());
		this.setNumerator(this.getDenominator(), false);
		this.setDenominator(oldNumerator, false);
		return this;
	}
	
}
