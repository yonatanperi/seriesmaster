package algebraicExpression;

class Arg {
	
	protected AlgebraicExpression argType, coefficient;	
	private char AEChar;
	
	Arg (AlgebraicExpression argType, AlgebraicExpression coefficient, char AEChar){
		this.setArg(argType, coefficient, AEChar);
	}
	
	Arg (AlgebraicExpression ae){
		char AEChar = ae.getAEChar();
		if (Arg.isArg(ae)) {
			this.setArg(SimplifyAlgebraicExpression.arrangeAE(ae));	
		}
		else {
			this.setArg(ae, AlgebraicExpression.get1AE(), AEChar);
		}
	}
	
	Arg(Arg arg){
		this.setArg(arg);
	}
	
	Arg() {}

	protected void setArg(Arg arg) {
		this.setArg(arg.getArgType(), arg.getCoefficient(), arg.getAEChar());
	}
	
	protected void setArg(AlgebraicExpression argType, AlgebraicExpression coefficient, char AEChar) {		
		this.setAEChar(AEChar);
		this.setArgType(argType);
		this.setCoefficient(coefficient);
	}

	static AlgebraicExpression[] argArraytoAEArray(Arg[] args) {
		AlgebraicExpression[] aes = new AlgebraicExpression[args.length];
		for (int i = 0; i < args.length; i++) {
			aes[i] = args[i].getAlgebraicExpression();
		}
		return aes;
	}
	
	/**
	 * Division is fliped multiplication.
	 */
	private Arg divideByArg(Arg arg) {
		if (Fraction.isFraction(arg) || Fraction.isFraction(this)) {
			return multiplyWithArg(new Fraction(arg.getAlgebraicExpression()).flip());
		}
		return new Fraction(this.getAlgebraicExpression(), arg.getAlgebraicExpression(), this.getAEChar());
	}
	
	protected Arg multiplyWithArg(Arg arg) {
		final Arg finalArg;
		if (arg.getClass() == Fraction.class) {
			finalArg = arg.multiplyWithArg(this);
		}
		else if (Fraction.isFraction(this)) {
			finalArg = new Fraction(this).multiplyWithArg(arg);
		}
		else {
			
			
			AlgebraicExpression coefficient = AlgebraicExpression.mergeAlgebraicExpressions(this.getCoefficient(), arg.getCoefficient(), '*');
			
			AlgebraicExpression argType;
			if (this.getArgType().toString().equals("1")) {
				argType = arg.getArgType();
			}
			else if (arg.getArgType().toString().equals("1")) {
				argType = this.getArgType();
			}
			else {
				argType = AlgebraicExpression.mergeAlgebraicExpressions(this.getArgType(), arg.getArgType(), '*', false);
			}
			
			finalArg = new Arg(argType, coefficient, this.getAEChar());					
		}
		return finalArg;
	}
	
	/**
	 * Subtraction 2 args is not actually a thing.
	 * Simply turning one of them to neg and then adding them.
	 */
	private Arg minusArg(Arg arg) {
		arg.getCoefficient().makeNeg();
		return this.addArg(arg);
	}

	protected Arg addArg(Arg arg) {
		
		if (arg.getClass() == Fraction.class) {
			return arg.addArg(this);
		}
		else if (Fraction.isFraction(this)) {
			return new Fraction(this).addArg(arg);
		}
		
		if (this.getArgType().equals(arg.getArgType(), false)) {
			Arg finalArg = new Arg(this);
			AlgebraicExpression coefficient;
			coefficient = AlgebraicExpression.mergeAlgebraicExpressions(this.getCoefficient(), arg.getCoefficient(), '+');
			finalArg.setCoefficient(coefficient);
			return finalArg;
		}
		return null;
	}
	
	/**
	 * Merge 2 args under one operator and simplify afterwards.
	 * @return the merged arg
	 */
	static Arg mergeArgs(Arg arg1, Arg arg2, char operator) {
		// returns (is succeeded?)
		
		if (arg1.isEmpty()) {
			return arg2;
		}
		if (arg2.isEmpty()) {
			return arg1;
		}
		
		if (AlgebraicExpression.isOperator(operator)) {			
			switch(operator) {
				case '+':
					return arg1.addArg(arg2);
				case '-':
					return arg1.minusArg(arg2);
				case '/':
					return arg1.divideByArg(arg2);
				case '*':
					return arg1.multiplyWithArg(arg2);
				case '#':
					return new Arg(SimplifyAlgebraicExpression.buildLog(
							arg1.getAlgebraicExpression(), arg2.getAlgebraicExpression(), arg2.getAEChar()));
			}
		}
		return null;
	}

	AlgebraicExpression getArgType() {
		return this.argType;
	}
	
	AlgebraicExpression getCoefficient() {
		return this.coefficient;
	}
	
	
	protected void setArgType(AlgebraicExpression argType) {
		this.argType = argType;
	}
	
	protected void setCoefficient(AlgebraicExpression coefficient) {
		this.coefficient = coefficient;
	}
	
	protected void setAEChar(char AEChar) {
		this.AEChar = AEChar;
		if (this.getArgType() != null && this.getCoefficient() != null) {
			this.argType.setAEChar(AEChar);
			this.coefficient.setAEChar(AEChar);
		}
	}
	
	AlgebraicExpression getAlgebraicExpression() {
		
		if (this.isEmpty()) {
			return new AlgebraicExpression();
		}
		
		if (this.getArgType().toString().equals("1")) { // just a num
			return this.getCoefficient();
		}
		if (this.getCoefficient().toString().equals("1")) { // coefficient is 1
			return this.getArgType();
		}
		if (this.getCoefficient().toString().equals("0")) { // coefficient is 0
			return this.getCoefficient();
		}
		
		return AlgebraicExpression.mergeAlgebraicExpressions(this.getCoefficient(), this.getArgType(), '*', false);	
	}
	
	static boolean isArg(AlgebraicExpression ae) {
		if (ae == null) {
			return false;
		}
		if (ae.isEmpty()) {
			return false;
		}
		
		return isArg(ae.getRoot());
	}
	
	private static boolean isArg(BinNode root) {
		if (AlgebraicExpression.isNegNum(root)) {
			return isArg(root.right);
		}
		return root.data != '+' && root.data != '-';
	}

	char getAEChar() {
		return this.AEChar;
	}

	boolean isEmpty() {
		if (this.getArgType() == null || this.getCoefficient() == null) {
			return true;
		}
		return this.getArgType().isEmpty() || this.getCoefficient().isEmpty();
	}
	
	boolean isNumeric() {
		if (this.isEmpty()) {
			return false;
		}
		return this.getArgType().toString().equals("1");
	}
	
	boolean isNeg() {
		if (this.getCoefficient() == null) {
			return false;
		}
		if (this.getCoefficient().isNumericExpression()) {
			if (Double.parseDouble(this.getCoefficient().toString()) < 0) {
				return true;
			}
		}
		return false;
	}
	
	Arg getAbsoluteValue() {
		if (this.isNeg()) {
			return new Arg(this.getArgType(), new AlgebraicExpression(this.getCoefficient().getRoot().right, this.getAEChar(), false), this.getAEChar());
		}
		return this;
	}
	
	public String toString() {
		return this.getAlgebraicExpression().toString();
	}
	
	public boolean equals(Object o) {
		
		if (o == null) {
			return false;
		}
		
		Arg arg = (Arg) o;
		return this.argType.equals(arg.argType) &&
				this.coefficient.equals(arg.coefficient);
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	
}
