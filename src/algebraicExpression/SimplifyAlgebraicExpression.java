package algebraicExpression;
import java.util.LinkedList;

import seriesTypes.Series;

import java.util.Hashtable;

import someHelpingFunctions.SomeHelpingFunctions;

import java.util.Arrays;


class SimplifyAlgebraicExpression {

	private final static short MAX_DEG = 5; 
	 
	static AlgebraicExpression simplify(AlgebraicExpression ae) {
		
		if (ae.isEmpty()) {
			return ae;
		}
		
		if (ae.isNumericExpression()) {
			double doubleAE = ae.calculate();
			AlgebraicExpression aeDouble = AlgebraicExpression.valueOf(doubleAE, ' ', false);
			
			if (doubleAE % 1 != 0) {
				return realSimplify(aeDouble);
			}
			if (!containsChar(ae, '/')) {
				return aeDouble;
			}
		}
		return realSimplify(ae);
	}

	private static AlgebraicExpression realSimplify(AlgebraicExpression ae) {
		
		ae = toFractions(ae);
		AlgebraicExpression[] algebraicExpressionArgs = divideToAlgebraicExpressionArgs(ae, true);		
		Arg[] args = simplifyArgs(algebraicExpressionArgs);
		args = collectLikeTerms(args);
		args = removeZeroes(args);
		
		AlgebraicExpression finalAE = AlgebraicExpression.mergeAlgebraicExpressions(Arg.argArraytoAEArray(args), '+', false);
		finalAE.setAEChar(ae.getAEChar());
		return finalAE;
	}
	
	private static AlgebraicExpression toFractions(AlgebraicExpression ae) {
		// 0.25n^2 -> n^2/4
		return fixMultiplation(new AlgebraicExpression(toFractions(ae.getRoot()), ae.getAEChar(), false));
	}

	private static BinNode toFractions(BinNode node) {
		// root - left - right
		
		if (node == null) {
			return null;
		}
		
		String strNode = AlgebraicExpression.getNumfromTree(node);
		if (SomeHelpingFunctions.isDouble(strNode)) {
			
			if (Double.parseDouble(strNode) % 1 == 0) {
				return node;
			}
			
			// The transformation
			node = new Fraction(new AlgebraicExpression(node, ' ', false)).getAlgebraicExpression().getRoot();
			return node;
		}
		if (!(node.data == '*' && node.right.data == '^' && node.right.left.toString().equals("10"))) {
			// It means: a*10^6, which doesn't need fraction transformation.
			node.left = toFractions(node.left);
		}		
		node.right = toFractions(node.right);		
		
		return node;
	}
	
	private static boolean containsChar(AlgebraicExpression ae, char c) {
		return containsChar(ae.getRoot(), c);
	}
	
	private static boolean containsChar(BinNode node, char c) {
		// left - root - right
		
		if (node == null) {
			return false;
		}
		
		if (node.data == c) {
			return true;
		}

		return containsChar(node.left, c) || containsChar(node.right, c);
	}
	
	private static Arg[] removeZeroes(Arg[] args) {
		// 6n+0 -> 6n
		if (args.length <= 1) {
			return args;
		}
		
		LinkedList<Arg> finalArgs = new LinkedList<Arg>();
		String argStr;
		for (Arg arg : args) {
			argStr = arg.getAlgebraicExpression().toString();
			if (!(argStr.equals("0") || argStr.equals("-0"))) {
				finalArgs.add(arg);
			}
		}
		
		if (finalArgs.isEmpty()) { // 0+0+0+...
			finalArgs.add(new Arg(new AlgebraicExpression("0", ' ', false)));
		}
		
		// convert the linkedList to array
		Object[] objArray = finalArgs.toArray();
		return Arrays.copyOf(objArray, objArray.length, Arg[].class);
	}

	private static String fixSigns(String SAE) {
		// Deals with: +-, ++, --, ...
		
		SAE = SAE.replace("++", "+");
		SAE = SAE.replace("--", "+");
		SAE = SAE.replace("+-", "-");
		SAE = SAE.replace("-+", "-");
		
		return SAE;
	}
	
	private static AlgebraicExpression[] divideToAlgebraicExpressionArgs(AlgebraicExpression ae, boolean simplifyParenthesis) {
		return divideToAlgebraicExpressionArgs(ae.toString(), ae.getAEChar(), simplifyParenthesis);
	}
	
	private static AlgebraicExpression[] divideToAlgebraicExpressionArgs(String SAE, char AEChar, boolean simplifyParenthesis) {	
		return divideToAlgebraicExpressionArgs(new LinkedList<AlgebraicExpression>(), fixSigns(SAE), AEChar, 0, 0, simplifyParenthesis);
	}
	
	private static AlgebraicExpression[] divideToAlgebraicExpressionArgs(LinkedList<AlgebraicExpression> dividedByArgs, String SAE, char AEChar, int fromIndex, int toIndex, boolean simplifyParenthesis) {
			
		if (toIndex >= SAE.length()) {

			dividedByArgs.add(new AlgebraicExpression(SAE.substring(fromIndex, SAE.length()), AEChar, false));
			
			// convert the linkedList to array
			return Arrays.copyOf(dividedByArgs.toArray(), dividedByArgs.size(), AlgebraicExpression[].class);
		}
		char currentChar = SAE.charAt(toIndex);
		if (currentChar == '(') {
			String parenthesisSubString = getParenthesisSubString(SAE, toIndex);
			if (simplifyParenthesis) {
				AlgebraicExpression replacement = new AlgebraicExpression(parenthesisSubString, AEChar);
				SAE = SAE.substring(0, toIndex) + SAE.substring(toIndex).replace("(" + parenthesisSubString + ")", "(" + replacement + ")");
				toIndex += replacement.toString().length() + 1;
			}
			else {
				toIndex += parenthesisSubString.length() + 1;
			}
		}
		else if (currentChar == '+') {
			dividedByArgs.add(new AlgebraicExpression(SAE.substring(fromIndex, toIndex), AEChar, false));
			fromIndex = toIndex + 1;
		}		
		else if (currentChar == '-' && toIndex != 0) {
			if (!AlgebraicExpression.isOperator(SAE.charAt(toIndex - 1)) && SAE.charAt(toIndex - 1) != '(') { // check for neg num
				dividedByArgs.add(new AlgebraicExpression(SAE.substring(fromIndex, toIndex), AEChar, false));
				fromIndex = toIndex;
			}
		}		
		return divideToAlgebraicExpressionArgs(dividedByArgs, SAE, AEChar, fromIndex, toIndex + 1, simplifyParenthesis);
	}
	
	private static Arg[] collectLikeTerms(Arg[] args) {
		
		if (args.length <= 1) {
			return args;
		}
		
		int fractionIndex = getFractionIndex(args);
		if (fractionIndex >= 0) {
			return new Fraction[] {collectToFraction(args, fractionIndex)};
		}
		
		LinkedList<Arg> collectedArgs = new LinkedList<Arg>();
		
		for (Arg arg : args) {
			int similarArgIndex = getSimilarArgTypeIndex(collectedArgs, arg);
			if (similarArgIndex >= 0) {
				
				Arg similarArg = collectedArgs.remove(similarArgIndex);
				
				collectedArgs.addFirst(Arg.mergeArgs(arg, similarArg, '+'));
			}
			else {
				
				collectedArgs.addFirst(arg);
			}
		}
		
		Object[] objArray = collectedArgs.toArray();
		return Arrays.copyOf(objArray, objArray.length, Arg[].class);
	}
	
	private static Fraction collectToFraction(Arg[] args, int fractionIndex) {
		Fraction finalFraction = (Fraction) args[fractionIndex];
		
		for (int i = 0; i < args.length; i++) {
			if (i != fractionIndex) {
				finalFraction = (Fraction) Arg.mergeArgs(finalFraction, args[i], '+');
			}
		}
		
		return finalFraction;
	}

	private static int getFractionIndex(Arg[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].getClass() == Fraction.class) {
				return i;
			}
		}
		return -1;
	}

	private static int getSimilarArgTypeIndex(LinkedList<Arg> args, Arg arg) {
		// Similar Arg is an arg with the same 'argType'.
		
		for (int index = 0; index < args.size(); index++) {
			if (args.get(index).getArgType().equals(arg.getArgType(), false)) {
				return index;
			}
		}
		return -1;
	}
	
	private static Arg[] simplifyArgs(AlgebraicExpression[] algebraicExpressionArgs) {
		return simplifyArgs(algebraicExpressionArgs, new LinkedList<Arg>(), 0);
	}
	
	private static Arg[] simplifyArgs(AlgebraicExpression[] algebraicExpressionArgs, LinkedList<Arg> args, int index) {
		// deals with (5+n)^2/3n...
		
		if (index >= algebraicExpressionArgs.length) {
			// convert the linkedList to array
			return Arrays.copyOf(args.toArray(), args.size(), Arg[].class);
		}
		
		algebraicExpressionArgs[index] = activateLogsRules(algebraicExpressionArgs[index]);
		if (!algebraicExpressionArgs[index].isEmpty()) { // If there was a: log a(neg num)
			
			algebraicExpressionArgs[index] = kibbutz(algebraicExpressionArgs[index]).getAlgebraicExpression();
			algebraicExpressionArgs[index] = reductAE(algebraicExpressionArgs[index]);
			if (!algebraicExpressionArgs[index].isEmpty()) { // If there was a/0
				if (algebraicExpressionArgs.length > 1) { // Only needed when there are multiple args
					// Open Parenthesis
					algebraicExpressionArgs[index] = contractedMultiplication(algebraicExpressionArgs[index]);
					algebraicExpressionArgs[index] = multiplierInsertion(algebraicExpressionArgs[index]);
				}
				
				AlgebraicExpression[] extendedAlgebraicExpressionArgs = divideToAlgebraicExpressionArgs(algebraicExpressionArgs[index], false);
				extendedAlgebraicExpressionArgs = kibbutz(extendedAlgebraicExpressionArgs);
				Arg[] extendedArgs = arrangeAEs(extendedAlgebraicExpressionArgs);
				extendedArgs = kibbutz(extendedArgs);
				extendedArgs = activatePowersRules(extendedArgs);
				extendedArgs = reductFractions(extendedArgs);		
				extendedArgs = kibbutz(extendedArgs);		
				
				SomeHelpingFunctions.addArrayToLinkedList(args, extendedArgs);
			}
		}
		return simplifyArgs(algebraicExpressionArgs, args, index + 1);
	}
	
	private static AlgebraicExpression activateLogsRules(AlgebraicExpression ae) {
		return new AlgebraicExpression(
				activateLogsRules(ae.getRoot()),
				ae.getAEChar(), false);
	}

	private static BinNode activateLogsRules(BinNode node) {
		// left - right - root
		
		if (node == null) {
			return null;
		}
		
		node.left = activateLogsRules(node.left);
		node.right = activateLogsRules(node.right);
		
		switch(node.data) {
			case '#':
				double left, right = 0;
				
				try {
					right = Double.parseDouble((AlgebraicExpression.getNumfromTree(node.right)));
					if (right <= 0) { // log a(x) = null | x <= 0
						return null;
					}					
					
				}
				catch (NumberFormatException e) {}
				
				try {
					left = Double.parseDouble((AlgebraicExpression.getNumfromTree(node.left)));
					if (left < 0 || left == 1) { // log a(x) = null | a < 0
						return null;
					}
					if (right != 0) { // Both side are numeric
						if ((left == 0 || left == 1) && left != right) { // log 0(not 0) = null || 1...
							return null;
						}
					}
				}
				
				catch (NumberFormatException e) {}
				
				if (node.right.data == '1' && node.right.left == null) { // log a(1) = 0
					return new BinNode('0');
				}
				if (node.left.equals(getPowerBase(node.right))) { // log a(a^x) = x
					return getPowerDeg(node.right);
				}							
				if (node.right.data == '*' || node.right.data == '/') { // log a(xy) = log a(x)+log a(y)
					BinNode newNode = new BinNode('+');					
					if (node.right.data == '/') {
						newNode.data = '_';
					}
					newNode.left = node;
					newNode.right = AlgebraicExpression.getBinNodeCopy(node);
					newNode.left.right = newNode.left.right.left;
					newNode.right.right = newNode.right.right.right;
					
					return activateLogsRules(newNode);
				}
				if (getPowerBase(node.left).equals(getPowerBase(node.right))) { // log a^n(a^m) = m/n
					BinNode newNode = new BinNode('/');
					newNode.left = getPowerDeg(node.right);
					newNode.right = getPowerDeg(node.left);
					return newNode;
				}
				if (node.right.data == '^') { // log a(x^b) = b*log a(x)
					BinNode newNode = new BinNode('*');
					newNode.left = node.right.right;
					newNode.right = node;
					node.right = node.right.left;
					
					return activateLogsRules(newNode);
				}
				break;
				
			case '^': // a^(log a(x)) = x
				if (node.right.data == '#') {
					if (node.left.equals(node.right.left)) {
						return node.right.right;
					}
				}
				break;
				
			case '/': // log b(x) / log b(a) = log a(x)
				if (node.left.data == '#' && node.right.data == '#') {
					if (node.left.left.equals(node.right.left)) {
						return activateLogsRules(buildLog(node.right.right, node.left.right));
					}
				}
		}
		return node;
	}
	
	static AlgebraicExpression buildLog(AlgebraicExpression base, AlgebraicExpression parenthesis, char AEChar) {
		return new AlgebraicExpression(
				buildLog(base.getRoot(), parenthesis.getRoot()),
				AEChar, false);
	}
	
	private static BinNode buildLog(BinNode base, BinNode parenthesis) {
		BinNode newNode = new BinNode('#');
		newNode.left = base;
		newNode.right = parenthesis;
		return newNode;
	}

	static AlgebraicExpression fixMultiplation(AlgebraicExpression argAE) {
		// a/bc -> ac/b
		
		fixMultiplation(argAE.getRoot());
		return argAE;
	}
	
	static BinNode fixMultiplation(BinNode node) {
		// left - right - root
		
		if (node != null) {
			fixMultiplation(node.left);
			fixMultiplation(node.right);
			
			if (node.data == '*') {
				if (node.left.data == '/' && node.right.data != '/') {
					BinNode nodeCopy = AlgebraicExpression.getBinNodeCopy(node); // Must work on a copy!
					BinNode temp = nodeCopy.right;
					nodeCopy.right = nodeCopy.left.right;
					nodeCopy.left.right = temp;
					nodeCopy.data = '/';
					nodeCopy.left.data = '*';
					
					node = nodeCopy;
				}
			}
		}
		
		return node;
	}

	private static Arg[] arrangeAEs(AlgebraicExpression[] aesArgs) {
		Arg[] args = new Arg[aesArgs.length];
		for (int i = 0; i < args.length; i++) {
			args[i] = arrangeAE(aesArgs[i]);
		}
		
		return args;
	}

	static Arg arrangeAE(AlgebraicExpression ae) {
		// n*a*n^2*4*a/3 -> 4*n*n^2*a*a/3
		char AEChar;
		if (ae.isEmpty()) {
			return new Arg();
		}
		if (containsAEChar(ae.getRoot(), ae.getAEChar())) {
			AEChar = ae.getAEChar();
		}
		else {
			AEChar = getAEChar(ae.getRoot());
		}
		
		return overAllMergeToArg(getAEDividedByCharsHash(ae), AEChar);
	}

	private static Arg overAllMergeToArg(Hashtable<Character, AlgebraicExpression> aeDividedByCharsHash, char AEChar) {
		// Over-All merge to Arg
		AlgebraicExpression finalCoefficient = new AlgebraicExpression();
		AlgebraicExpression finalArgType = null;
		AlgebraicExpression currentAE;
				
		for (Character c : aeDividedByCharsHash.keySet()) {
			currentAE = aeDividedByCharsHash.get(c);
			if (c == AEChar) { // Will be true only one time
				if (c == ' ' && !containsChar(currentAE, '#') && !containsChar(currentAE, '^')) {
					finalCoefficient = AlgebraicExpression.mergeAlgebraicExpressions(finalCoefficient, currentAE, '*', false);
					finalArgType = AlgebraicExpression.get1AE();
				}
				else {
					finalArgType = currentAE;
				}
			}
			else {
				finalCoefficient = AlgebraicExpression.mergeAlgebraicExpressions(finalCoefficient, currentAE, '*', false);
			}
		}
		if (finalCoefficient.isEmpty()) {
			finalCoefficient = AlgebraicExpression.get1AE();
		}
		if (finalArgType == null) {
			finalArgType = finalCoefficient;
			finalCoefficient = AlgebraicExpression.get1AE();
		}
		
		
		return new Arg(finalArgType, finalCoefficient, AEChar);
	}

	static Hashtable<Character, AlgebraicExpression> getAEDividedByCharsHash(AlgebraicExpression ae) {
		Hashtable<Character, LinkedList<AlgebraicExpression>> aeDividedByCharsHash = getAEDividedByCharsHash(ae.getRoot(), new Hashtable<Character, LinkedList<AlgebraicExpression>>());
		Hashtable<Character, AlgebraicExpression> mergedDividedByCharsAEs = new Hashtable<Character, AlgebraicExpression>();
		
		// In hash merge
		Object[] objArray;
		
		for (Character c : aeDividedByCharsHash.keySet()) {
			// convert the linkedList to array
			objArray = aeDividedByCharsHash.get(c).toArray();
			mergedDividedByCharsAEs.put(c, activatePowersRules(AlgebraicExpression.mergeAlgebraicExpressions(Arrays.copyOf(objArray, objArray.length, AlgebraicExpression[].class), '*', false)));
		}
		
		return mergedDividedByCharsAEs;
	}
	
	private static Hashtable<Character, LinkedList<AlgebraicExpression>> getAEDividedByCharsHash(BinNode node, Hashtable<Character, LinkedList<AlgebraicExpression>> aeDividedByCharsHash) {
		// Preorder traversal
		// Sets aeDividedByCharsHash
		
		if (node != null) {
			
			if (AlgebraicExpression.isNegNum(node) && !AlgebraicExpression.isNumericExpression(node)) {
				aeDividedByCharsHash.put(' ', new LinkedList<AlgebraicExpression>());
				aeDividedByCharsHash.get(' ').add(new AlgebraicExpression("-1", ' ', false));
				return getAEDividedByCharsHash(node.right, aeDividedByCharsHash);
			}
			else if (node.data == '/' && !AlgebraicExpression.getNumfromTree(node.left).equals("1")) {
				node = toFractionMultiplation(node);	
			}
						
			if (node.data == '*') {
				// Recursion
				getAEDividedByCharsHash(node.left, aeDividedByCharsHash);
				getAEDividedByCharsHash(node.right, aeDividedByCharsHash);
			}
			else {
				// Adding to aeDividedByCharsHash
				char AEChar = getAEChar(node);
				if (!aeDividedByCharsHash.containsKey(AEChar)) {
					aeDividedByCharsHash.put(AEChar, new LinkedList<AlgebraicExpression>());					
				}
				aeDividedByCharsHash.get(AEChar).add(new AlgebraicExpression(node, AEChar, false));
			}
		}
		
		return aeDividedByCharsHash;
	}
	
	
	
	private static BinNode toFractionMultiplation(BinNode node) {
		// a/b -> a*(1/b)
		
		if (node.data == '/' && !AlgebraicExpression.getNumfromTree(node.left).equals("1")) {
			BinNode newRight = new BinNode('/');
			newRight.left = new BinNode('1');
			newRight.right = node.right;
		
			node.data = '*';
			node.right = newRight;
		}
		return node;
	}
	
	static boolean containsAEChar(BinNode node, char AEChar) {
		// left - right - root
		
		if (node != null) {
			boolean left, right;
			left = containsAEChar(node.left, AEChar);
			right = containsAEChar(node.right, AEChar);
			
			if (node.data == AEChar) {
				return true;
			}
			return left || right;
		}
		
		return false;
	}
	
	static char getAEChar(BinNode node) {
		if (containsOneAEChar(node)) {
			return findAEChar(node);
		}
		return '!'; // Unknown AEChar
	}
	
	static boolean containsOneAEChar(BinNode node) {
		return containsOneAEChar(node, new LinkedList<Character>());
	}
	
	private static boolean containsOneAEChar(BinNode node, LinkedList<Character> AEChars) {
		// left - right - root
		
		if (node != null) {
			containsOneAEChar(node.left, AEChars);
			containsOneAEChar(node.right, AEChars);
			
			if (Character.isLetter(node.data) && !AEChars.contains(node.data)) {
				AEChars.add(node.data);
			}
			return AEChars.size() < 2;
		}
		
		return true;
	}
	
	private static char findAEChar(BinNode node) {
		// left - right - root
		
		if (node == null) {
			return ' ';
		}
		
		char left, right;
		left = findAEChar(node.left);
		right = findAEChar(node.right);
		
		if (left != ' ') {
			return left;
		}
		else if (right != ' ') {
			return right;
		}
		else {
			if (Character.isLetter(node.data)) {
				return node.data;
			}
			return left; // return ' '
		}
	}
	
	private static Arg[] reductFractions(Arg[] args) {
		for (int index = 0; index < args.length; index++) {			
			if (Fraction.getFirstDivisionSign(args[index].getAlgebraicExpression()) != null &&
					args[index].getClass() != Fraction.class) { // Is Fraction				
				args[index] = new Fraction(args[index]);
			}
		}
		return args;
	}
	
	private static AlgebraicExpression reductAE(AlgebraicExpression ae) {
		if (Fraction.getFirstDivisionSign(ae) != null) { // Is Fraction				
			return new Fraction(ae).getAlgebraicExpression();
		}
		return ae;
	}

	private static Arg[] activatePowersRules(Arg[] args) {
		// Activate powers rules on the coefficient and the argType separately.
		
		for (int i = 0; i < args.length; i++) {
			args[i].setCoefficient(activatePowersRules(args[i].getCoefficient()));
			args[i].setArgType(activatePowersRules(args[i].getArgType()));
		}
		return args;
	}
	
	private static AlgebraicExpression activatePowersRules(AlgebraicExpression argAE) {
		return new AlgebraicExpression(
				activatePowersRules(argAE.getRoot()),
				argAE.getAEChar(), false);
	}
	
	private static BinNode activatePowersRules(BinNode node) {
		// Postorder traversal
		
		if (node == null) {
			return null;
		}
		
		node.left = activatePowersRules(node.left);
		node.right = activatePowersRules(node.right);		
		
		if (node.data == '^') {
						
			if (node.left.data == '0' && node.left.left == null) { // 0^n = 0
				node = new BinNode('0');
			}
			else if (node.right.data == '0' && node.right.left == null || node.left.data == '1' && node.left.left == null) { // a^0 || 1^n = 1
				node = new BinNode('1');
			}
			else if (node.right.data == '1' && node.right.left == null) { // a^1 = a
				node = node.left;
			}
			else if (AlgebraicExpression.isNegNum(node.left) &&
					SomeHelpingFunctions.isDouble(AlgebraicExpression.getNumfromTree(node.right))) { // (-a)^2n = a^2n
				if (Double.parseDouble(AlgebraicExpression.getNumfromTree(node.right)) % 2 == 0) {
					node.left = node.left.right;				
				}
			}
			else if (node.left.data == '^') { // (a^n)^m = a^(n*m)
				BinNode newNode = new BinNode('^');
				newNode.left = node.left.left;
				newNode.right = new BinNode('*');
				newNode.right.left = node.left.right;
				newNode.right.right = node.right;
				newNode.right= new AlgebraicExpression(newNode.right).getRoot(); // Simplify
				node = newNode;	
			}
			else if (node.left.data == '*' || node.left.data == '/') { // (a*b)^n = a^n*b^n
				BinNode newNode = new BinNode(node.left.data);
				newNode.left = AlgebraicExpression.getBinNodeCopy(node);
				newNode.right = AlgebraicExpression.getBinNodeCopy(node);
				newNode.left.left = newNode.left.left.left;
				newNode.right.left = newNode.right.left.right;
				node = newNode;
			}
			else if (AlgebraicExpression.isNegNum(node.right)) { // a^(-n) = 1/a^n
					BinNode newNode = new BinNode('/');
					newNode.left = new BinNode('1');
					newNode.right = new BinNode('^');
					newNode.right.left = node.left;
					newNode.right.right = new BinNode('*');
					newNode.right.right.left = AlgebraicExpression.setNumInTree("-1");
					newNode.right.right.right = node.right;
					newNode.right.right = new AlgebraicExpression(newNode.right.right).getRoot(); // Simplify
					node = newNode;
			}			
			else {
				Double leftValue, rightValue;
				leftValue = new AlgebraicExpression(node.left, false).calculate();
				rightValue = new AlgebraicExpression(node.right, false).calculate();
				
				if (leftValue == null || rightValue == null) { // no adjustments needed
					return node;
				}
				else if (rightValue > MAX_DEG) {
					return node;
				}
				else { // calculate numeric ae
					node = AlgebraicExpression.setNumInTree(String.valueOf(
							Math.pow(leftValue, rightValue)));
				}
			}					
			return activatePowersRules(node);
		}
		
		if (node.data == '*' || node.data == '/') {
			
			/*
			 * a^n*a^m = a^(n+m)
			 * a^n/a^m = a^(n-m)
			 */
			
			BinNode leftPowerBase = getPowerBase(node.left);
			
			if (leftPowerBase.equals(getPowerBase(node.right))) {
				BinNode newNode = new BinNode('^');
				newNode.left = leftPowerBase;
				switch (node.data) {
					case '*':
						newNode.right = new BinNode('+');
						break;
					case '/':
						newNode.right = new BinNode('_');
				}				
				newNode.right.left = getPowerDeg(node.left);
				newNode.right.right = getPowerDeg(node.right);
				
				newNode.right = new AlgebraicExpression(newNode.right).getRoot(); // Simplify
				
				return activatePowersRules(newNode);
			}
		}
		
		return node;
	}

	static BinNode _ToMinus1Multiplation(BinNode node) {
		if (node.data == '_') {			
			node.data = '+';
			BinNode newRight = new BinNode('*');
			newRight.left = AlgebraicExpression.setNumInTree("-1");
			newRight.right = node.right;
			if (AlgebraicExpression.getNumfromTree(node.left).equals("0")) {
				node = newRight;
			}
			else {
				node.right = newRight;
			}
		}
		return node;
	}
	
	private static AlgebraicExpression multiplierInsertion(AlgebraicExpression argAE) {
		// a*(c+b) -> a*c+a*b
		return fixMultiplation(new AlgebraicExpression(
				multiplierInsertion(argAE.getRoot()),
				argAE.getAEChar(), false));
	}
	
	private static BinNode multiplierInsertion(BinNode node) {
		// Postorder traversal
		
		if (node == null) {
			return null;
		}
		
		node.left = multiplierInsertion(node.left);
		node.right = multiplierInsertion(node.right);
		
		if (node.data == '_') {			
			return multiplierInsertion(_ToMinus1Multiplation(node));
		}
		
		if (node.data == '*' || node.data == '/') {
			
			BinNode[] sides = {node.left, node.right}; 
			for (int sideIndex = 0; sideIndex < 2; sideIndex++) {
				if ((sides[sideIndex].data == '+' || (AlgebraicExpression.isNeg(sides[sideIndex].data) && !AlgebraicExpression.isNegNum(sides[sideIndex]))) && 
						 !(node.data == '/' && sideIndex == 1) && !removeMultiplicationNeutrals(node)) {
					
					sides[1] = toFractionMultiplation(node).right; // (a+b)/c -> (a+b)*(1/c)
					
					if (sides[sideIndex].data == '-') {
						sides[sideIndex].data = '_';
					}
					BinNode newRoot = new BinNode(sides[sideIndex].data);
					BinNode[] newRootSides = new BinNode[2];
					BinNode[] sideSides = {sides[sideIndex].left, sides[sideIndex].right};
					
					AlgebraicExpression ae1 = new AlgebraicExpression(sides[1 - sideIndex], 'n', false);
					AlgebraicExpression ae2;
					
					for (int newRootSideIndex = 0; newRootSideIndex < 2; newRootSideIndex++) {
						
						
						ae2 = new AlgebraicExpression(sideSides[newRootSideIndex], 'n', false);
						// The AEChar doesn't matter.
						if (Fraction.isFraction(ae1) && Fraction.isFraction(ae2)) { // Fractions multiplication...
							Fraction f1 = new Fraction(ae1);
							Fraction f2 = new Fraction(ae2);
							
							newRootSides[newRootSideIndex] = Arg.mergeArgs(f1, f2, '*').getAlgebraicExpression().getRoot();
							continue;
						}
						
						// Creating the sides
						newRootSides[newRootSideIndex] = new BinNode('*');
						newRootSides[newRootSideIndex].left = AlgebraicExpression.getBinNodeCopy(sides[1 - sideIndex]);						
						
						if (AlgebraicExpression.isNegNum(sides[1 - sideIndex])) { // -a*-b = a*b
							if (AlgebraicExpression.isNegNum(sideSides[newRootSideIndex])) {
								newRootSides[newRootSideIndex].left = newRootSides[newRootSideIndex].left.right;
								sideSides[newRootSideIndex] = sideSides[newRootSideIndex].right;
							}
							else if (AlgebraicExpression.isNeg(newRoot.data) && newRootSideIndex == 1) {
								newRootSides[newRootSideIndex].left = newRootSides[newRootSideIndex].left.right;
								newRoot.data = '+';
							}
						}
						
											
						newRootSides[newRootSideIndex].right = sideSides[newRootSideIndex];
					}
					newRoot.left = multiplierInsertion(newRootSides[0]);
					newRoot.right = multiplierInsertion(newRootSides[1]);
					node = newRoot;
					break;
				}				
			}
		}
		
		return node;
	}
	
	/**
	 * Remove neutrals
	 * @return If successful, return true
	 */
	private static boolean removeMultiplicationNeutrals(BinNode node) {		
		
		if (node.data == '*') {
			
			BinNode[] sides = {node.left, node.right};
			String strSide;
			
			for (int sideIndex = 0; sideIndex < 2; sideIndex++) {
				strSide = AlgebraicExpression.getNumfromTree(sides[sideIndex]);
				if (strSide.equals("0")) {
					node = sides[sideIndex];
					return true;
				}
				if (strSide.equals("1")) {
					node = sides[1 - sideIndex];
					return true;
				}
			}
		}
		
		return false;
	}
	
	private static AlgebraicExpression contractedMultiplication(AlgebraicExpression argAE) {
		// (a+b)^2 -> a^2+2ab+b^2
		return new AlgebraicExpression(
				contractedMultiplication(argAE.getRoot(), argAE.getAEChar()),
				argAE.getAEChar(), false);		
	}
	
	private static BinNode contractedMultiplication(BinNode node, char AEChar) {
		// Inorder traversal
		
		if (node == null) {
			return null;
		}
		if (node.data == '^') {
			if (Character.isDigit(node.right.data)) {
				double rightNode = Double.parseDouble(AlgebraicExpression.getNumfromTree(node.right));
				if (rightNode % 1 == 0) {
					
					switch(node.left.data) {
						case '-':
							if (!AlgebraicExpression.isNegNum(node.left)) {
								// set as a neg num
								BinNode addingSign = new BinNode('+');
								addingSign.left = node.left.left;
								addingSign.right =  AlgebraicExpression.setNumInTree("-" + rightNode);
								node.left = addingSign;
								return contractedMultiplication(node, AEChar);

							}
							break;
						case '+':
							int deg = (int) rightNode;
							int[] coefficientsArray = getCoefficientsArray(deg);
							AlgebraicExpression[] finalArgs = new AlgebraicExpression[deg + 1];
							BinNode newRoot;
							
							for (int index = 0; index < finalArgs.length; index++) {
								newRoot = new BinNode('*');
								newRoot.left = AlgebraicExpression.setNumInTree(String.valueOf(coefficientsArray[index]));
								
								newRoot.right = new BinNode('*');
								newRoot.right.left = new BinNode('^');
								newRoot.right.right = new BinNode('^');
								newRoot.right.left.left = new BinNode(node.left.left);
								newRoot.right.right.left = new BinNode(node.left.right);
								newRoot.right.left.right = AlgebraicExpression.setNumInTree(String.valueOf(deg - index));
								newRoot.right.right.right = AlgebraicExpression.setNumInTree(String.valueOf(index));
								finalArgs[index] = new AlgebraicExpression(newRoot, AEChar);
							}
							return AlgebraicExpression.mergeAlgebraicExpressions(finalArgs, '+', false).getRoot();
					}
				}
			}
		}
		node.left = contractedMultiplication(node.left, AEChar);
		node.right = contractedMultiplication(node.right, AEChar);
		return node;
	}
	
	private static int[] getCoefficientsArray(int deg){
		int[][] pascal = new int[deg + 1][deg + 1];
		for (int i = 0; i < deg + 1; i++) {
			pascal[i][0] = pascal[i][i] = 1;
			for (int j = 1; j < i; j++) {
				pascal[i][j] = pascal[i-1][j-1] + pascal[i-1][j];
			}			
		}
		return pascal[deg];
	}
	
	private static AlgebraicExpression[] kibbutz(AlgebraicExpression[] argAes) {
		for (int i = 0; i < argAes.length; i++) {
			argAes[i] = kibbutz(argAes[i]).getAlgebraicExpression();
		}
		return argAes;
	}
	
	private static Arg[] kibbutz(Arg[] args) {
		// Kibbutzes the coefficient and the argType separately.
		for (int i = 0; i < args.length; i++) {
			args[i].setCoefficient(kibbutz(args[i].getCoefficient()).getAlgebraicExpression());
			args[i].setArgType(kibbutz(args[i].getArgType()).getAlgebraicExpression());
		}
		return args;
	}
	
	private static Arg kibbutz(AlgebraicExpression argAe) {
		if (Arg.isArg(argAe)) {
			return kibbutz(argAe.toString(), argAe.getAEChar());
		}
		return new Arg(argAe);
	}
	
	private static Arg kibbutz(String argSAE, char AEChar) {
		Arg arg = new Arg();
		arg.setAEChar(AEChar);
		arg = kibbutz(argSAE, arg, ' ', 0, 0);
		arg.setAEChar(AEChar);
		return arg;
	}
	
	private static Arg kibbutz(String argSAE, Arg finalArg, char prevOperator, int fromIndex, int toIndex) {
		
		char currentChar;	
		
		if (toIndex >= argSAE.length()) {
			if (finalArg.isEmpty()) {
				currentChar = '!';
			}
			else {
				if (prevOperator != '!') {
					finalArg = Arg.mergeArgs(finalArg, new Arg(new AlgebraicExpression(argSAE.substring(fromIndex, argSAE.length()), finalArg.getAEChar(), false)), prevOperator);
				}
				return finalArg;
			}
		}
		else {
			currentChar = argSAE.charAt(toIndex);
		}
		if (currentChar == '(') {			
			toIndex += getParenthesisSubString(argSAE, toIndex).length() + 1;
		}
		else {
			switch(currentChar) {
				case '*':
				case '/':
				case '!':
				case '#':
					
					Arg newArg = new Arg(new AlgebraicExpression(argSAE.substring(fromIndex, toIndex), finalArg.getAEChar(), false));
					// Remove neutrals							
					if (!newArg.getAlgebraicExpression().toString().equals("1") || toIndex == 1) {
						if (fromIndex == 0) {
							finalArg = newArg;
						}
						else {
							if (prevOperator == '#') { // log
								if (finalArg.isNumeric() && newArg.isNumeric()) {
									finalArg = new Arg(AlgebraicExpression.valueOf((Math.log(finalArg.getAlgebraicExpression().calculate()) / Math.log(newArg.getAlgebraicExpression().calculate())), finalArg.getAEChar()));
								}
							}
							else {
								finalArg = Arg.mergeArgs(finalArg, newArg, prevOperator);							
							}
						}
					}
			
					prevOperator = currentChar;
					fromIndex = toIndex + 1;
					break;
			}
		}
		

		return kibbutz(argSAE, finalArg, prevOperator, fromIndex, toIndex + 1);
	}
	
	static AlgebraicExpression simplifyNumericAE(AlgebraicExpression ae) {
		Double calculatedAE = ae.calculate();
		if (calculatedAE != null) {
			return AlgebraicExpression.valueOf(String.valueOf(calculatedAE), ae.getAEChar(), false);	
		}
		return ae;
	}
	
	private static String getParenthesisSubString(String s, int parenthesisIndex) {
		return getParenthesisSubString(s, parenthesisIndex, parenthesisIndex + 1, 1);
	}
	
	private static String getParenthesisSubString(String s, int parenthesisIndex, int charIndex, int parenthesisNumIndex) {
		if (parenthesisNumIndex == 0) {
			return s.substring(parenthesisIndex + 1, charIndex - 1);
		}
		
		if (charIndex >= s.length()) {
			return "";
		}
		
		if (s.charAt(charIndex) == '(') {
			parenthesisNumIndex++;
		}
		else if (s.charAt(charIndex) == ')') {
			parenthesisNumIndex--;
		}
		
		return getParenthesisSubString(s, parenthesisIndex, charIndex + 1, parenthesisNumIndex);
	}
	
	static double noa(double a, double b) {

		if (b == 0) {
			return a;
		}
		
		if (a % 1 != 0) {			
			double expendBy = (int) Math.pow(10, ((int) Math.log10(SomeHelpingFunctions.flipByDot(a % 1, Series.ROUND_PLACES)) + 1));
			return ((double) 1 / expendBy) * (noa(expendBy * a, expendBy * b));
		}
		
		if (b % 1 != 0) {
			return noa(b, a);
		}		
		
		return noa(b, a % b);
	}
	
	static double noa(double arr[]) { 
		double noasCoefficient = arr[0]; 
        for (int i = 1; i < arr.length; i++){ 
        	noasCoefficient = noa(arr[i], noasCoefficient); 
  
            if(noasCoefficient == 1) 
            { 
               return 1; 
            } 
        }
        return noasCoefficient; // ����� ������
    }
	
	static AlgebraicExpression[] noa(AlgebraicExpression ae) {
		// ����� ���� ����� ������
		// returns {���� �����, the rest of AE}
		
		if (ae == null) {
			return null;
		}
		
		AlgebraicExpression[] argAEs = divideToAlgebraicExpressionArgs(ae, false);
		LinkedList<AlgebraicExpression> extendedAEArgs = new LinkedList<AlgebraicExpression>(); // temp pointer
		
		for (int i = 0; i < argAEs.length; i++) {
			SomeHelpingFunctions.addArrayToLinkedList(extendedAEArgs, divideToAlgebraicExpressionArgs(multiplierInsertion(argAEs[i]), false));			
		}		
		
		// convert the linkedList to array
		AlgebraicExpression[][] midResult = noa(Arrays.copyOf(extendedAEArgs.toArray(), extendedAEArgs.size(), AlgebraicExpression[].class), ae.getAEChar());
		/*if (midResult[0][0].equals(AlgebraicExpression.get1AE(), false)) {
			midResult = noa(argAEs, ae)
		}*/
		
		AlgebraicExpression[] finalResult = new AlgebraicExpression[2];
		finalResult[0] = midResult[0][0];
		finalResult[1] = AlgebraicExpression.mergeAlgebraicExpressions(midResult[1], '+');
		
		if (finalResult[0].equals(AlgebraicExpression.get1AE(), false)) {
			// Replace
			AlgebraicExpression temp = finalResult[0];
			finalResult[0] = finalResult[1];
			finalResult[1] = temp;
		}
		
		return finalResult;
	}
	
	static AlgebraicExpression[][] noa(AlgebraicExpression[] argAEs, char AEChar) {
		
		if (argAEs.length == 1) {
			return new AlgebraicExpression[][] {argAEs, new AlgebraicExpression[] {AlgebraicExpression.valueOf(1, ' ', false)}};
		}
		
		Hashtable<Character, AlgebraicExpression>[] aesDividedByCharsHash = getAEsDividedByCharsHash(argAEs);
		
		AlgebraicExpression[] keyAEs;
		
		// Let's kick it!
		LinkedList<AlgebraicExpression> noas = new LinkedList<AlgebraicExpression>(); // ������ �������
		for (char c : aesDividedByCharsHash[0].keySet()) { // Sets noas
			if (allContainsChar(aesDividedByCharsHash, c)) {

				keyAEs = new AlgebraicExpression[aesDividedByCharsHash.length];
				
				for (int i = 0; i < aesDividedByCharsHash.length; i++) { // Sets keyAEs
					keyAEs[i] = aesDividedByCharsHash[i].get(c);
					if (keyAEs[i] == null) {
						keyAEs[i] = aesDividedByCharsHash[i].get('!');
					}
				}
				
				if (c == ' ') { // number
					/*
					 * TODO
					 * Change the rounding here!
					 */
					double[] coefficients = new double[aesDividedByCharsHash.length];
					for (int i = 0; i < coefficients.length; i++) { // Sets coefficients
						coefficients[i] = keyAEs[i].calculate();
					}
					
					double noasCoefficient = noa(coefficients);
					//double noasCoefficient = SomeHelpingFunctions.roundNum(noa(coefficients), Series.ROUND_PLACES);
					
					// Sets the new coefficients						
					for (int i = 0; i < keyAEs.length; i++) {
						keyAEs[i].setAlgebraicExpression(AlgebraicExpression.valueOf(SomeHelpingFunctions.roundNum(coefficients[i] / noasCoefficient, Series.ROUND_PLACES), c, false), false);
					}
					
					noas.addFirst(AlgebraicExpression.valueOf(noasCoefficient, c, false));
				}
				else { // not a number
					SomeHelpingFunctions.addArrayToLinkedList(noas, takeOutPowerNoa(keyAEs));
					
					// TODO
					/*// Reset aesDividedByCharsHash
					for (int i = 0; i < argAEs.length; i++) {
						argAEs[i] = overAllMergeToArg(aesDividedByCharsHash[i], argAEs[i].getAEChar()).getAlgebraicExpression();							
					}
					
					aesDividedByCharsHash = getAEsDividedByCharsHash(argAEs);*/
				}
			}
		}
		
		AlgebraicExpression[][] finalResult = new AlgebraicExpression[2][argAEs.length];

		// convert the linkedList to array
		if (noas.isEmpty()) {
			finalResult[0][0] = AlgebraicExpression.get1AE();
		}
		else {
			Object[] objNoasArray = noas.toArray();
			finalResult[0][0] = AlgebraicExpression.mergeAlgebraicExpressions(Arrays.copyOf(objNoasArray, objNoasArray.length, AlgebraicExpression[].class), '*', false);
		}
		
		AlgebraicExpression[] theRestOfAEs = new AlgebraicExpression[aesDividedByCharsHash.length];
		for (int i = 0; i < aesDividedByCharsHash.length; i++) {
			theRestOfAEs[i] = overAllMergeToArg(aesDividedByCharsHash[i], AEChar).getAlgebraicExpression();
		}

		
		finalResult[1] = theRestOfAEs;
		
		return finalResult; // {{mergedNoas}, {theRestOfAEs}}
	}
	
	private static Hashtable<Character, AlgebraicExpression>[] getAEsDividedByCharsHash(AlgebraicExpression[] argAEs) {
		Hashtable<Character, AlgebraicExpression>[] aesDividedByCharsHash = (Hashtable<Character, AlgebraicExpression>[]) new Hashtable[argAEs.length];

		for (int i = 0; i < aesDividedByCharsHash.length; i++) { // Sets aesDividedByCharsHash
			aesDividedByCharsHash[i] = getAEDividedByCharsHash(argAEs[i]);			
		}
		
		return aesDividedByCharsHash;
	}
	
	private static AlgebraicExpression[] takeOutPowerNoa(AlgebraicExpression[] keyAEs) {
		AlgebraicExpression[] lowestDegPowers = getLowestDegPowers(keyAEs);
		LinkedList<BinNode>[] allPowers = getAllPowers(keyAEs);		
		
		// temp
		BinNode newDeg;
		
		for (AlgebraicExpression lowestDegPower : lowestDegPowers) {
			for (LinkedList<BinNode> powersList : allPowers) {
				for (BinNode power : powersList) {				
					if (getPowerBase(power).equals(getPowerBase(lowestDegPower.getRoot()))) {
						newDeg = new BinNode('_');
						newDeg.left = getPowerDeg(power);
						newDeg.right = getPowerDeg(lowestDegPower.getRoot());
						newDeg = new AlgebraicExpression(newDeg).getRoot(); // Simplify
						setDeg(power, newDeg);
						power.setBinNode(activatePowersRules(power));						
					}
				}
			}			
		}
						
		
		return lowestDegPowers;
	}
	
	private static AlgebraicExpression[] getLowestDegPowers(AlgebraicExpression[] aes) {
		
		LinkedList<LinkedList<BinNode>> allNoaPowers = getAllNoaPowers(aes);
		AlgebraicExpression[] lowestDegPowers = new AlgebraicExpression[allNoaPowers.size()];
		
		BinNode lowestDegPower;
		int index = 0;
		
		for (LinkedList<BinNode> powersList : allNoaPowers) {
			lowestDegPower = powersList.removeFirst();
			
			for (BinNode power : powersList) {
				if (new AlgebraicExpression(getPowerDeg(lowestDegPower), false).compareTo(
						new AlgebraicExpression(getPowerDeg(power), false)) == 1) {
					lowestDegPower = power;
				}
			}
			lowestDegPowers[index] = new AlgebraicExpression(AlgebraicExpression.getBinNodeCopy(lowestDegPower), false);
			index++;
		}
		
		return lowestDegPowers;
	}

	private static LinkedList<LinkedList<BinNode>> getAllNoaPowers(AlgebraicExpression[] aes) {
		LinkedList<LinkedList<BinNode>> finalPowersList = new LinkedList<LinkedList<BinNode>>();
		for (BinNode currentPower : getAllPowers(aes[0])) { // Sets finalPowersList
			finalPowersList.addFirst(new LinkedList<BinNode>());
			finalPowersList.getFirst().add(currentPower);
		}
		
		// temps
		LinkedList<BinNode> currentPowersList;
		boolean toDeleteFinalPower;
		
		for (int i = 1; i < aes.length; i++) {
			currentPowersList = getAllPowers(aes[i]);
			for (int powerIndex = 0; powerIndex < finalPowersList.size(); powerIndex++) { // Do not touch
				toDeleteFinalPower = true;
				for (BinNode currentPower : currentPowersList) {
					if (getPowerBase(finalPowersList.get(powerIndex).getFirst()).equals(getPowerBase(currentPower))) {
						finalPowersList.get(powerIndex).addFirst(currentPower);
						currentPowersList.remove(currentPower);
						toDeleteFinalPower = false;
					}
				}
				if (toDeleteFinalPower) {
					finalPowersList.remove(powerIndex);
					powerIndex--;
				}
			}			
		}
		
		return finalPowersList;
	}
	
	private static BinNode getPowerBase(BinNode node) {
		if (node.data == '^') {
			return node.left;
		}
		return node;
	}
	
	private static BinNode getPowerDeg(BinNode node) {
		if (node.data == '^') {
			return node.right;
		}
		return new BinNode('1');
	}
	
	private static BinNode setDeg(BinNode node, BinNode deg) {
		if (node.data == '^') {
			node.right = deg;		
		}
		else {
			BinNode newNode = new BinNode('^');
			newNode.left = AlgebraicExpression.getBinNodeCopy(node);
			newNode.right = deg;
			node.setBinNode(newNode);
			return newNode;
		}
		return node;
	}
	
	private static LinkedList<BinNode>[] getAllPowers(AlgebraicExpression[] aes) {
		LinkedList<BinNode>[] allPowers = (LinkedList<BinNode>[]) new LinkedList[aes.length];
		for (int i = 0; i < aes.length; i++) {
			allPowers[i] = getAllPowers(aes[i]);
		}
		return allPowers;
	}
	
	private static LinkedList<BinNode> getAllPowers(AlgebraicExpression ae) {
		LinkedList<BinNode> powersList = getAllPowers(ae.getRoot(), new LinkedList<BinNode>());
		if (powersList.isEmpty()) {
			powersList.add(ae.getRoot());
		}
		return powersList;
	}
	
	private static LinkedList<BinNode> getAllPowers(BinNode node, LinkedList<BinNode> powersList) {
		// root - left - right
		
		if (node == null) {
			return null;
		}
		
		if (node.data == '^') {
			powersList.add(node);
			return powersList;
		}
		
		if (node.data == '*') {
			getAllPowers(node.left, powersList);
			getAllPowers(node.right, powersList);
		}		
		
		return powersList;
	}
	
	private static boolean allContainsChar(Hashtable<Character, AlgebraicExpression>[] aesDividedByCharsHash, Character c) {
		boolean flag = true;
		for (int i = 0; i < aesDividedByCharsHash.length; i++) {
			if (!aesDividedByCharsHash[i].containsKey(c)) {
				if (aesDividedByCharsHash[i].containsKey('!')) {
					if (!SimplifyAlgebraicExpression.containsAEChar(aesDividedByCharsHash[i].get('!').getRoot(), c)) {						
						flag = false;
					}
				}
				else {
					flag = false;
				}
			}
		}
		return flag;
	}

}
