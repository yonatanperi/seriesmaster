package algebraicExpression;

class BinNode{
	char data;
	BinNode left, right;
	
	BinNode(char data) {
		this.data = data;
		this.left = this.right = null;
	}
	
	BinNode(BinNode binNode) {
		this.setBinNode(binNode);
	}
	
	void setBinNode(BinNode binNode) {
		this.data = binNode.data;
		this.left =  binNode.left;
		this.right =  binNode.right;
	}
	
	public String toString() {
		return AlgebraicExpression.toString(this, false);
	}
	
	public boolean equals(Object o) {
		// root - left - right
		
		if (o == null) {
			return false;
		}
		
		BinNode node = (BinNode) o;		
		if (this.data == node.data) {
			if (this.left == null && node.left == null && this.right == null && node.right == null) {
				return true;
			}
			if ((this.left == null && node.left != null) || (this.right == null && node.right != null)) {
				return false;
			}
			if (this.left == null && node.left == null) {
				return this.right.equals(node.right);
			}
			if (this.right == null && node.right == null) {
				return this.left.equals(node.left);
			}
			if (this.data == '+' || this.data == '*') { // The change rule
				if (this.left.equals(node.right) && this.right.equals(node.left)) {
					return true;
				}
			}
			return this.left.equals(node.left) && this.right.equals(node.right);
		}
		return false;
	}
}
