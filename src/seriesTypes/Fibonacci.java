package seriesTypes;
import algebraicExpression.AlgebraicExpression;

public class Fibonacci extends Series{
	
	// a1 is in super class
	private final AlgebraicExpression a2;
	private char seriesOperator;
	
	public Fibonacci(String[] args) {
		super(args);
		this.a2 = this.getGivenArgs()[1];
		this.seriesOperator = this.findSeriesOperator(this.getGivenArgs());
	}
	
	private char findSeriesOperator(AlgebraicExpression[] args) {
		for (char operator : AlgebraicExpression.getAllOperators()) {
			if (this.trackSeries(args, operator)) {
				return operator;
			}
		}
		return '!'; // Coudln't find series operator
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		return this.trackSeries(args, this.seriesOperator);
	}

	private boolean trackSeries(AlgebraicExpression[] args, char seriesOperator) {
		if (this.seriesOperator == '!') {
			return false;
		}
		
		if (args.length >= 3) {
			if (AlgebraicExpression.mergeAlgebraicExpressions(args[0], args[1], seriesOperator).equals(args[2])){
				return trackSeries(Series.popFirstArg(args), seriesOperator);
			}
			return false;
		}
		return true;
	}
	public AlgebraicExpression getValue(int n) {
		if (n == 1) {
			return this.getA1();
		}
		if (n == 2) {
			return this.a2;
		}
		AlgebraicExpression a1, a2, returnValue;
		if (n < 1) {
			a1 = getValue(n + 1);
			a2 = getValue(n + 2);
			returnValue = AlgebraicExpression.mergeAlgebraicExpressions(a2, a1, AlgebraicExpression.getCounterOperator(this.seriesOperator));
		}
		else {
			a1 = getValue(n - 1);
			a2 = getValue(n - 2);
			returnValue = AlgebraicExpression.mergeAlgebraicExpressions(a1, a2, this.seriesOperator);
		}
		return returnValue;
	}
	
	public String getStringAlgebraicExpression() {
		return String.format("a(n - 1) %s a(n - 2)", this.seriesOperator);
	}
	
	public void printSeriesRange(int initialIndex, int finalIndex) {
		printSeriesRange(finalIndex - initialIndex, this.getValue(initialIndex), this.getValue(initialIndex + 1));
	}
	
	private void printSeriesRange(int index, AlgebraicExpression n1, AlgebraicExpression n2) {
		System.out.print(n1 + ", ");
		if (index > 0) {
			printSeriesRange(index - 1, n2, AlgebraicExpression.mergeAlgebraicExpressions(n1, n2, this.seriesOperator));
		}
		else {
			System.out.print("...");
		}
	}
	
}
