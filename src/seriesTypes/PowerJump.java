package seriesTypes;

import algebraicExpression.AlgebraicExpression;

public class PowerJump extends BaseSeries {
	
	// a, a^d, a^(d^2), ...
	
	public PowerJump(String[] args) {
		super(args);
	}
	
	protected void setSeries(AlgebraicExpression a1, AlgebraicExpression difference) {
		this.setAn(new AlgebraicExpression(String.format("(%s)^((%s)^(n-1))", a1, difference), 'n'));
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		if (args.length >= 3) {
			AlgebraicExpression log1 = AlgebraicExpression.mergeAlgebraicExpressions(args[0], args[1], '#');
			AlgebraicExpression log2 = AlgebraicExpression.mergeAlgebraicExpressions(args[1], args[2], '#');
			if (!(log1.isEmpty() || log2.isEmpty()) && log1.equals(log2, false)) { // Also checks for log rules violation
				return trackSeries(Series.popFirstArg(args));
			}
			return false;
		}
		this.setSeries();
		return true;
		
	}
	
	protected AlgebraicExpression findDifference(AlgebraicExpression[] args) {
		return AlgebraicExpression.mergeAlgebraicExpressions(args[0], args[1], '#');
	}
	
}
