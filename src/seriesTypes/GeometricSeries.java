package seriesTypes;

import algebraicExpression.AlgebraicExpression;

public class GeometricSeries extends BaseSeries{
	
	// a, da, 2da, ...
	
	public GeometricSeries(String[] args) {
		super(args);
	}

	protected void setSeries(AlgebraicExpression a1, AlgebraicExpression difference) {
		this.setAn(new AlgebraicExpression(String.format("(%s)*(%s)^(n-1)", a1, difference), 'n'));
		this.setSum(new Series(String.format("((%s)*((%s)^n-1))/(%s-1)", a1, difference, difference)));
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		if (args.length >= 3) {
			if (new AlgebraicExpression("(" + args[1] + ")^2").equals(
					AlgebraicExpression.mergeAlgebraicExpressions(args[0], args[2], '*'), false)) {
				return trackSeries(Series.popFirstArg(args));
			}
			return false;
		}
		this.setSeries();
		return true;
		
	}
	
	protected AlgebraicExpression findDifference(AlgebraicExpression[] args) {
		return AlgebraicExpression.mergeAlgebraicExpressions(args[1], args[0], '/');
	}
	
	public AlgebraicExpression getInfinitSum() {
		Double calculatedDifference = this.getDifference().calculate();
		if (calculatedDifference != null) {
			if (calculatedDifference < 1) {
				return AlgebraicExpression.mergeAlgebraicExpressions(this.getA1(), AlgebraicExpression.valueOf(1 - calculatedDifference), '/');
			}
		}
		
		return null;
	}
	
}
