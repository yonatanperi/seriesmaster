package seriesTypes;

import algebraicExpression.AlgebraicExpression;
import someHelpingFunctions.SomeHelpingFunctions;


public class Series {
	
	/**
	 * #.######## -> #.###
	 */
	public static final int ROUND_PLACES = 3;
	
	/**
	 * The user inserted  Args.
	 */
	private AlgebraicExpression[] givenArgs;
	
	/**
	 * The first arg of the series.
	 */
	private AlgebraicExpression a1;
	
	/**
	 * The difference between args.
	 */
	private AlgebraicExpression difference;
	
	/**
	 * a(n), as a function.
	 */
	private AlgebraicExpression an;
	
	/**
	 * The sum of the series�s args from a1 to a(n).
	 */
	private Series sum;
	
	/**
	 * Constructs an empty series.
	 */
	public Series() {}
	
	/**
	 * Constructs a series from an SAE (String AE).
	 */
	public Series(String SAE) {
		this(new AlgebraicExpression(SAE, 'n'));	
	}
	
	/**
	 * Constructs a series from AE.
	 */
	public Series(AlgebraicExpression AE) {
		this.setAn(AE);
	}
	
	/**
	 * A copy constructor.
	 * @param series is the origin series.
	 */
	public Series(Series series) {
		this(series.getAlgebraicExpression());
		this.setSum(series.getSum());
	}
	
	/**
	 * Construct a series from args.
	 */
	public Series(String[] args) {		
		this(SomeHelpingFunctions.toAEArray(args));
	}
	
	/**
	 * Construct a series from args.
	 */
	public Series(AlgebraicExpression[] args) {
		this.givenArgs = args;
		this.a1 = this.givenArgs[0];
	}

	// Set & Get
	
	public String getSeriesType() {
		return this.getClass().getSimpleName();
	}
	
	public AlgebraicExpression getDifference() {		
		return this.difference;
	}
	
	public AlgebraicExpression getValue(int n) {
		return this.getAlgebraicExpression().getValue(n);
	}
	
	protected AlgebraicExpression getSum(int n) {
		if (this.hasSumSeries()) {
			return this.getSum().getValue(n);
		}
		return AlgebraicExpression.mergeAlgebraicExpressions(getSum(n - 1), this.getValue(n), '+');
	}
	
	public String getANStringAlgebraicExpression() {
		return "a(n) = " + this.getStringAlgebraicExpression();
	}
	
	public String getStringAlgebraicExpression() {
		if (this.getAlgebraicExpression() == null) {
			return "Coudn't find elementary algebraic expression.";
		}
		return this.getAlgebraicExpression().toString();
	}
	
	public AlgebraicExpression getAlgebraicExpression() {
		return this.an;
	}
	
	protected void setSum(Series sum) {
		this.sum = sum;		
	}
	
	public Series getSum() {
		return this.sum;
	}
	
	public AlgebraicExpression getInfinitSum() {
		return null;
	}
	
	public int getIntValue(int n) {
		return (int) (double) Double.parseDouble(this.getValue(n).toString());
	}
	
	protected void setDifference(AlgebraicExpression difference) {
		this.difference = difference;
	}
	
	protected AlgebraicExpression[] getGivenArgs() {
		return this.givenArgs;
	}
	
	protected void setAn(AlgebraicExpression an) {
		this.an = an;
	}
	
	protected AlgebraicExpression getA1() {
		return this.a1;
	}
	
	// Some staff
	
	public void printSeriesRange(int initialIndex, int finalIndex) {
		if (initialIndex <= finalIndex) {
			System.out.print(this.getValue(initialIndex) + ", ");
			printSeriesRange(initialIndex + 1, finalIndex);
		}
		else {
			System.out.print(this.getValue(initialIndex) + "...");
		}
	}
	
	private boolean doubleCheck() {
		AlgebraicExpression[] copyArgs = new AlgebraicExpression[this.getGivenArgs().length];
		for (int argIndex = 0; argIndex < copyArgs.length; argIndex++) {
			copyArgs[argIndex] = this.getValue(argIndex + 1);
		}
		
		return AlgebraicExpression.equals(copyArgs, this.getGivenArgs());
	}
	
	protected static AlgebraicExpression[] popFirstArg(AlgebraicExpression[] args) {
		AlgebraicExpression[] newArgs = new AlgebraicExpression[args.length - 1];
		for (int i = 1; i < args.length; i++) {
			newArgs[i - 1] = args[i];
		}
		return newArgs;
	}
	
	private static boolean areLegitArgs(AlgebraicExpression[] args) {
		for (AlgebraicExpression arg : args) {
			if (arg == null) {
				return false;
			}
			if (arg.isEmpty()) {
				return false;
			}
		}
		
		return true;
	}
	
	public boolean hasSumSeries() {
		return this.getSum() != null;
	}
	
	// Over-rides
	
	public String toString() {
		String s = "";
		
		String seriesType = this.getSeriesType();		
		if (seriesType.equals("Series")) {
			seriesType = "Some"; // Will be "Some Series" 
		}
		else {
			seriesType = seriesType.replace("Series", "");
		}
		
		s += "This series is " + seriesType + " Series";
		if (this.getDifference() != null) {
			s += "\nThe difference is " + this.getDifference();
		}
		s += "\nThe first 3 arguments are: ";
		for (int n = 1; n <= 3; n++) {
			s += this.getValue(n) + ", ";
		}
		s += "...";
		s += "\nThe algebraic expression of the series is: " + this.getANStringAlgebraicExpression();

		return s;
	}
	
	
	protected void setSeries() {
		// Activate when track series is successful.
		// Sets the an, sum and difference.
	}
	
	public boolean trackSeries() {
		// If success, returns true.
		
		if (this.getGivenArgs().length > 2 && areLegitArgs(this.getGivenArgs())) {
			if (this.trackSeries(this.getGivenArgs())){
				if (this.doubleCheck()) {
					if (!this.hasSumSeries()) {
						this.setSum(new SumSeries(this));
					}
					return true;
				}
			}
		}
		
		return false;
	}

	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		// over-ride!
		return false;
	}
	
	
	
	protected AlgebraicExpression findDifference(AlgebraicExpression[] args) {
		return null;
	}
	
	public boolean equals(Object o) {
		
		if (o == null) {
			return false;
		}
		
		return this.getAlgebraicExpression().equals(((Series) o).getAlgebraicExpression());
	}
	
	public int hashCode() {
		return this.getAlgebraicExpression().hashCode();
	}
}
