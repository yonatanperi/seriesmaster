package seriesTypes;
import algebraicExpression.AlgebraicExpression;
import seriesAnalysis.TrackSeries;
import someHelpingFunctions.SomeHelpingFunctions;

public class CyclicJumps extends Series {
	
	// EX: 2, 5, 3, 10, 4, 20, ...
	
	private Series[] serieses;
	
	public CyclicJumps(String[] args) {
		super(args);
	}
	
	public AlgebraicExpression getValue(int n) {
		
		int seriesIndex = Math.abs(n) % serieses.length;
		if (seriesIndex == 0) {
			seriesIndex = serieses.length;
		}
		seriesIndex--;
		
		int index;
		if (n > 0) {
			index = n / serieses.length + 1;
			if (n % serieses.length == 0) {
				index--;
			}			
		}
		else{
			index = n / serieses.length;
		}
		return this.serieses[seriesIndex].getValue(index);
	}
	
	public String getStringAlgebraicExpression() {
		String s = "\n";
		for (int seriesIndex = 0; seriesIndex < this.serieses.length; seriesIndex++) {
			s += String.format("%s. ", seriesIndex + 1) + this.serieses[seriesIndex].getStringAlgebraicExpression() + "\n";
		}
		return s;
	}

	protected boolean trackSeries(AlgebraicExpression[] args) {
		
		AlgebraicExpression[][] seriesesArgsDivision;
		Series[] serieses;
		Series series;
		boolean standartSeries;
		
		for (int jumps = 2; jumps <= args.length / 2; jumps++) {
			standartSeries = true;
			seriesesArgsDivision = CyclicJumps.seriesesDivision(args, jumps);
			serieses = new Series[jumps];
			for (int seriesIndex = 0; seriesIndex < seriesesArgsDivision.length; seriesIndex++) {
				series = TrackSeries.trackSeriesExceptTypes(SomeHelpingFunctions.toStringArray(seriesesArgsDivision[seriesIndex]), new String[] {"CyclicJumps", "Polynomial"});
				if (series == null) {
					standartSeries = false;
				}
				serieses[seriesIndex] = series;
			}
			if (standartSeries) {
				this.serieses = serieses;
				return true;
			}			
		}
		return false;
	}
	
	private static AlgebraicExpression[][] seriesesDivision(AlgebraicExpression[] args, int jumps) {
		AlgebraicExpression[][] cyclicArgs = new AlgebraicExpression[jumps][args.length / jumps];
		int jumpIndex;
		for (int row = 0; row < jumps; row++) {
			jumpIndex = row;
			for (int column = 0; column < args.length / jumps; column++) {
				cyclicArgs[row][column] = args[jumpIndex];
				jumpIndex += jumps;
			}
		}
		return cyclicArgs;
	}
}
