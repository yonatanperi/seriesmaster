package seriesTypes;

import algebraicExpression.AlgebraicExpression;

class BaseSeries extends Series{
	
	public BaseSeries(String[] args) {
		super(args);
	}
	
	public BaseSeries(AlgebraicExpression a1, AlgebraicExpression difference) {
		super();
		this.setSeries(a1, difference);
	}
	
	protected void setSeries(AlgebraicExpression a1, AlgebraicExpression difference) {
		// Override!
	}
	
	protected void setSeries() {
		this.setDifference(this.findDifference(this.getGivenArgs()));
		this.setSeries(this.getGivenArgs()[0], this.getDifference());
	}
}
