package seriesTypes;

import algebraicExpression.AlgebraicExpression;

class SumSeries extends Series {
	
	private final Series originSeries;
	
	public SumSeries(Series series) {
		this.originSeries = series;
	}
	
	public AlgebraicExpression getValue(int n) {
		if (n <= 0) {
			return new AlgebraicExpression();
		}
		
		return AlgebraicExpression.mergeAlgebraicExpressions(this.getValue(n - 1), this.originSeries.getValue(n), '+');
	}
	
	public String getStringAlgebraicExpression() {
		return "a(n-1) + a(n-2) + ... | n >= 0";
	}
	
	public Series getSum() {
		return new SumSeries(this);
	}
}
