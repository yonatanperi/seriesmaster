package seriesTypes;

import algebraicExpression.AlgebraicExpression;
import seriesAnalysis.TrackSeries;
import someHelpingFunctions.SomeHelpingFunctions;

public class DotSplit extends Series {

	// EX: 12.3, 14.6, 16.12
	
	private Series[] seriesesdividedByDot;
	
	public DotSplit(String[] args) {
		super(args);
	}
	
	private double getDoubleValue(int n) {
		double beforeDotValue = this.seriesesdividedByDot[0].getValue(n).calculate();
		double afterDotValue = this.seriesesdividedByDot[1].getValue(n).calculate();
		return beforeDotValue + afterDotValue / Math.pow(10, (double) SomeHelpingFunctions.getIntLength((int) afterDotValue));
	}
	
	public AlgebraicExpression getValue(int n) {
		return AlgebraicExpression.valueOf(this.getDoubleValue(n));
	}
	
	public String getStringAlgebraicExpression() {
		return String.format("{%s}.{%s}", this.seriesesdividedByDot[0].getStringAlgebraicExpression(), this.seriesesdividedByDot[1].getStringAlgebraicExpression());		
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		if (SomeHelpingFunctions.areAllNumericAE(args)) {
			return trackSeries(SomeHelpingFunctions.AEArrayToDoubleArray(args));
		}
		return false;
	}
	
	private boolean trackSeries(double[] args) {
		if (SomeHelpingFunctions.isAllInt(args)) {
			return false;
		}
		int[][] argsDividedByDot = DotSplit.divideArgsByDot(args);
		Series[] seriesesdividedByDot = new Series[argsDividedByDot.length];
		String[] exceptSeriesTypes = {
				"DotSplit",
				"Polynomial"
		};
		for (int i = 0; i < argsDividedByDot.length; i++) {
			seriesesdividedByDot[i] = TrackSeries.trackSeriesExceptTypes(SomeHelpingFunctions.intArrayToStringArray(argsDividedByDot[i]), exceptSeriesTypes);
			if (seriesesdividedByDot[i] == null) {
				return false;
			}
		}
		this.seriesesdividedByDot = seriesesdividedByDot;
		return true;
	}
	
	static int[][] divideArgsByDot(double[] args) {
		// returns: [[Before dot], [After dot]]
		if (SomeHelpingFunctions.isAllInt(args)) {
			return new int[][] {SomeHelpingFunctions.doubleArrayToIntArray(args), new int[args.length]};
		}
		int[][] newArgs = new int[2][args.length];
		for (int i = 0; i < args.length; i++) {
			newArgs[0][i] = (int) args[i];
			newArgs[1][i] = DotSplit.getAfterDot(args[i]);
		}
		return newArgs;
	}
	
	public static int getAfterDot(double num) {
		return DotSplit.getAfterDot(String.valueOf(num), 0);
	}
	
	private static int getAfterDot(String num, int digitIndex) {
		if (num.charAt(digitIndex) == '.') {
			num = num.substring(digitIndex + 1);
			return Integer.parseInt(num);
		}
		return DotSplit.getAfterDot(num, digitIndex + 1);
	}
	
}
