package seriesTypes;

import algebraicExpression.AlgebraicExpression;

public class ArithmeticSeries extends BaseSeries {
	
	// a, a + d, a + 2d, ...
	
	public ArithmeticSeries(String[] args) {
		super(args);
	}

	protected void setSeries(AlgebraicExpression a1, AlgebraicExpression difference) {
		this.setAn(new AlgebraicExpression(String.format("%s+(%s)*(n-1)", a1, difference), 'n'));
		this.setSum(new Series(String.format("(%s+%s)*n/2", a1, this.getAlgebraicExpression().toString())));
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		if (args.length >= 3) {
			if (new AlgebraicExpression("2*(" + args[1] + ')').equals(
					AlgebraicExpression.mergeAlgebraicExpressions(args[0], args[2], '+'))) {
				return trackSeries(Series.popFirstArg(args));
			}
			return false;
		}
		this.setSeries();
		return true;
	}
	
	protected AlgebraicExpression findDifference(AlgebraicExpression[] args) {
		return AlgebraicExpression.mergeAlgebraicExpressions(args[1], args[0], '-');
	}
	
}
