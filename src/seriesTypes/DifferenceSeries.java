package seriesTypes;
import algebraicExpression.AlgebraicExpression;
import seriesAnalysis.TrackSeries;
import someHelpingFunctions.SomeHelpingFunctions;

public class DifferenceSeries extends Series {
	
	private Series differenceSumSeries;
	
	public DifferenceSeries(String[] args) {
		super(args);
	}
	
	public AlgebraicExpression getValue(int n) {
		return AlgebraicExpression.mergeAlgebraicExpressions(this.getA1(), this.differenceSumSeries.getValue(n - 1), '+');
	}
	
	public String getStringAlgebraicExpression() {
		return this.getA1() + "+" + this.differenceSumSeries.getStringAlgebraicExpression();
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		
		Series differenceSeries = TrackSeries.trackSeriesExceptTypes(getDifferenceArgs(args, '-'), new String[] {
				"DifferenceSeries",
				"SumSeries"
		});		
		if (differenceSeries != null) {
			if (differenceSeries.hasSumSeries()) {
				this.differenceSumSeries = differenceSeries.getSum();
				if (this.differenceSumSeries.hasSumSeries()) {
					this.setSum(new Series(new AlgebraicExpression(
							String.format("n(%s)+%s", this.getA1(), this.differenceSumSeries.getSum().getAlgebraicExpression()), 'n')));
				}
				return true;
			}
		}		
		return false;
	}
	
	private static String[] getDifferenceArgs(AlgebraicExpression[] args, char operator) {
		AlgebraicExpression[] differenceArgs = new AlgebraicExpression[args.length - 1];
		for (int i = 0; i < differenceArgs.length; i++) {			
			differenceArgs[i] = AlgebraicExpression.mergeAlgebraicExpressions(args[i + 1], args[i], operator);
		}
		return SomeHelpingFunctions.toStringArray(differenceArgs);

	}

}
