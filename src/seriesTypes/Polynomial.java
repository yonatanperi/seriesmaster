package seriesTypes;

import algebraicExpression.AlgebraicExpression;
import someHelpingFunctions.SomeHelpingFunctions;

public class Polynomial extends Series {
	
	// EX: 2, 5, 10, 17, ... | a(n) = n^2 + 1
	
	public Polynomial(String[] args) {
		super(args);
		this.setAn(this.createNewPolynomial(SomeHelpingFunctions.stringArrayToDoubleArray(args)));
	}
	
	private static double[][] createMat(double[] args) {
		double[][] mat = new double[args.length][args.length + 1];
		for (int i = 0; i < args.length; i++) {
			mat[i][args.length] = args[i];
			
			for (int j = 0; j < args.length; j++) {
				mat[i][j] = Math.pow(i + 1, args.length - 1 - j);
			}
		}
		return mat;
	}
	
	private AlgebraicExpression createNewPolynomial(double[] args) {
		
		double[][] coefficientMat = createMat(args);
		for (int row = 0; row < coefficientMat.length; row ++) {
			devideRow(coefficientMat[row], coefficientMat[row][row]);
			for (int column = 0; column < coefficientMat.length; column ++) {
				if (column != row) {
					double multiplyBy = coefficientMat[column][row];
					minusRowByRow(coefficientMat[column], multiplyRow(coefficientMat[row], multiplyBy));
					devideRow(coefficientMat[row], multiplyBy);
				}
			}
		}
		
		String SAE = "";		
		
		for (int row = 0; row < coefficientMat.length; row++) {
			if (coefficientMat[row][coefficientMat.length] + 1 == 1) {
				coefficientMat[row][coefficientMat.length] = 0;
			}
			SAE += String.format("%s*n^%s+", SomeHelpingFunctions.roundNum(coefficientMat[row][coefficientMat.length], ROUND_PLACES), coefficientMat.length - 1 - row);
		}
		SAE += "0";
		
		return new AlgebraicExpression(SAE);
		
	}
	
	private static double[] multiplyRow(double[] row, double n) {
		for (int index = 0; index < row.length; index++) {
			row[index] *= n;
		}
		return row;
	}
	
	private static double[] devideRow(double[] row, double n) {
		for (int index = 0; index < row.length; index++) {
			row[index] /= n;
		}
		return row;
	}
	
	private static double[] minusRowByRow(double[] row1, double[] row2) {
		// row1 - row2
		for (int index = 0; index < row1.length; index++) {
			row1[index] -= row2[index];
		}
		return row1;
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		return true;
	}
	
}
