package seriesTypes;

import seriesAnalysis.TrackSeries;
import someHelpingFunctions.SomeHelpingFunctions;

import algebraicExpression.AlgebraicExpression;

public class CyclicAppearance extends Series {
	
	// EX: 415, 41515, 4151515, ...
	
	private String appearance;
	private Series NumberOfAppearanceSeries;
	private String[] leftOvers; // [left, right]
	
	public CyclicAppearance(String[] args) {
		super(args);
	}
	
	protected void setSeries(Series NumberOfAppearanceSeries, String[] leftOvers) {
		this.NumberOfAppearanceSeries = NumberOfAppearanceSeries;
		this.leftOvers = leftOvers;
		this.setDifference(this.findDifference());		
	}
	
	public AlgebraicExpression getValue(int n) {
		String copiedAppearance = this.leftOvers[0] + CyclicAppearance.copyAppearance(this.appearance, this.NumberOfAppearanceSeries.getIntValue(n)) + this.leftOvers[1];
		if (copiedAppearance.length() == 0) {
			return null;
		}
		return new AlgebraicExpression(copiedAppearance);
	}
	
	private AlgebraicExpression findDifference() {
		return this.NumberOfAppearanceSeries.getDifference();
	}
	
	public String getStringAlgebraicExpression() {
		return (this.leftOvers[0] + String.format("(%s**{%s})", this.appearance, this.NumberOfAppearanceSeries.getStringAlgebraicExpression()) + this.leftOvers[1]);		
	}
	
	protected boolean trackSeries(AlgebraicExpression[] args) {
		if (SomeHelpingFunctions.areAllNumericAE(args)) { // Only numeric args can be traced as cyclicAppearance series.
			return trackSeries(SomeHelpingFunctions.toStringArray(SomeHelpingFunctions.toInts(args)));
		}
		return false;
	}
	
	/**
	 * -Overload-
	 * @param args are String type. The CyclicAppearance tracking system works only with String.
	 */
	private boolean trackSeries(String[] args) {
		String[] leftOvers = CyclicAppearance.getLeftOvers(args);
		if (leftOvers == null) {
			return false;
		}
		CyclicAppearance.removeLeftOvers(args, leftOvers);
		String appearance = String.valueOf(args[1]);
		Series NumberOfAppearanceSeries = this.trackNumberOfAppearanceSeries(args, appearance);
		if (NumberOfAppearanceSeries != null) {
			this.setSeries(NumberOfAppearanceSeries, leftOvers);
			return true;
		}	
		return false;
	}
	
	/**
	 * Just remove the leftOvers from both sides of the args.
	 */
	public static void removeLeftOvers(String[] args, String[] leftOvers){
		for (int argIndex = 0; argIndex < args.length; argIndex++) {
			int[] appearanceRange = {leftOvers[0].length(), args[argIndex].length() - leftOvers[1].length()};
			args[argIndex] = args[argIndex].substring(appearanceRange[0], appearanceRange[1]);
		}
	}
	
	private static String[] getLeftOvers(String[] args){
		return getLeftOvers(args, new String[] {args[0], ""}, false);
	}
	
	/**
	 * 
	 * @param args are the user args.
	 * @param leftOvers the arts & crafts of the function to find the leftOvers.
	 * starts with the values: [args[0], ""], witch will become [left side leftOver, right side leftOver].
	 * @param side: false - left, true - right. Starts with the left side.
	 * @return the leftOvers if succeeded, null if not.
	 */
	private static String[] getLeftOvers(String[] args, String[] leftOvers, boolean side){
			
		String currentLeftOver;
		
		if (side) {
			// left -> right
			leftOvers[1] = leftOvers[1].substring(1, leftOvers[1].length());
			currentLeftOver = leftOvers[1];
		}
		else {
			// right -> left
			leftOvers[0] = leftOvers[0].substring(0, leftOvers[0].length() - 1);
			currentLeftOver = leftOvers[0];
		}
		
		if (leftOvers[0].length() == 0) { // Failed
			return null;
		}
		
		if (isLeftOver(args, currentLeftOver, side) || leftOvers[1].length() == 0 && side) {
			if (side) {
				// Done.
				return leftOvers;
			}
			
			// Switch side!
			leftOvers[1] = args[0].substring(leftOvers[0].length() - 1);
			return getLeftOvers(args, leftOvers, true);
		}
		
		// Recursion
		return getLeftOvers(args, leftOvers, side);
	}
	
	/**
	 * 
	 * @param args will be checked for having leftOver.
	 * @param leftOver is the leftOver witch will be checked by.
	 * @param side: false - left, true - right.
	 * @return if all the args have the leftOver (if the leftOver is legit).
	 */
	private static boolean isLeftOver(String[] args, String leftOver, boolean side) {
		
		for (String arg : args) {
			if (!(side && arg.substring(arg.length() - leftOver.length()).equals(leftOver) || 
					!side && arg.substring(0, leftOver.length()).equals(leftOver))) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Tracks how many times the certain checkedAppearance appears on the appearanceArgs.
	 * @param appearanceArgs are the args without left overs.
	 * @param checkedAppearance is the appearance
	 * @return the series which represents the appearance appearances in the args.
	 */
	private Series trackNumberOfAppearanceSeries(String[] appearanceArgs, String checkedAppearance) {
		
		if (checkedAppearance.length() == 0) {
			return null;
		}
		
		Series cyclicAppearanceSeries = CyclicAppearance.trackCyclicAppearanceSeries(appearanceArgs, checkedAppearance);
		
		if (cyclicAppearanceSeries == null) {
			for (short side = 0; side < 2; side++) { // side: left - 0, right - 1
				Series sideSeries = this.trackNumberOfAppearanceSeries(appearanceArgs, checkedAppearance.substring(side, checkedAppearance.length() - 1 + side));
				if (sideSeries != null) {
					this.appearance = checkedAppearance;
					return sideSeries;
				}
			}
		}
		else {
			return cyclicAppearanceSeries;
		}
		
		return null;		
	}
	
	// ("45", 3) --> 454545
	private static String copyAppearance(String appearance, int copiesNumer) {
		return CyclicAppearance.copyAppearance(appearance, appearance, copiesNumer);
	}
	
	private static String copyAppearance(String appearance, String currentAppearance, int copiesNumer) {		
		if (copiesNumer == 1) {
			return currentAppearance;
		}
		if (copiesNumer < 1) {
			return "";
		}
		return CyclicAppearance.copyAppearance(appearance, currentAppearance + appearance, copiesNumer - 1);
	}
	
	private static int[] getNumberOfAppearancesInArgs(String[] args, String checkedAppearance) {
		int[] numberOfAppearancesInArgs = new int[args.length];
		for (int i = 0; i < numberOfAppearancesInArgs.length; i++) {
			numberOfAppearancesInArgs[i] = CyclicAppearance.getNumberOfAppearances(String.valueOf(args[i]), checkedAppearance);
		}
		return numberOfAppearancesInArgs;
	}
	
	private static Series trackCyclicAppearanceSeries(String[] args, String checkedAppearance) {
		return TrackSeries.trackSeriesExceptTypes(
				SomeHelpingFunctions.intArrayToStringArray(
						CyclicAppearance.getNumberOfAppearancesInArgs(args, checkedAppearance)),
							new String[] {"CyclicAppearance", "DotSplit", "Polynomial"});		
	}
	
	private static int getNumberOfAppearances(String num, String checkedAppearance) {
		return CyclicAppearance.getNumberOfAppearances(num, checkedAppearance, 0, 1);
	}
	
	private static int getNumberOfAppearances(String num, String checkedAppearance, int digitIndex, int checkedDigitIndex) {
		if (digitIndex == num.length()) {
			return 0;
		}
		if (checkedDigitIndex > num.length()) {
			return getNumberOfAppearances(num, checkedAppearance, digitIndex + 1, digitIndex + 2);
		}		
		if (checkedAppearance.equals(num.substring(digitIndex, checkedDigitIndex))) {
			return getNumberOfAppearances(num, checkedAppearance, digitIndex + 1, digitIndex + 2) + 1;
		}
		return getNumberOfAppearances(num, checkedAppearance, digitIndex, checkedDigitIndex + 1);
	}
	
		
}
