package userExperience;
import seriesTypes.Series;
import java.util.Scanner;

import algebraicExpression.AlgebraicExpression;

public class SeriesMenu {
	
	private static String[] MENU = {
			"Find argument by location",
			"Move to SUM series",
			"Print series range",
			"Print series algebraic expretion",
			"Get Infinit Sum",
			"Track new series",
			"Exit"
			};
	
	private final static Scanner io = new Scanner(System.in);
	
	static void printWelcomePage(Series userSeries) {
		System.out.println("Series is Standart!");
		System.out.println(userSeries);
		System.out.println("\nIsn't it the series you thought of?");
		System.out.println("Jast press " + (MENU.length - 1) + " and add more args of your series.");
	}
	
	static void openMenu(Series series) throws Exception {
		
		// Prints the menu
		System.out.println("\n\nmenu: ");
		for (int i = 0; i < MENU.length; i++) {
			System.out.println((i + 1) + ". " + MENU[i]);
		}
					
		System.out.print("\nEnter a number of command: ");
		int userCommandNum = io.nextInt();
		System.out.print("\n");
		
		switch (userCommandNum){
			case 1:
				System.out.print("Enter the location: ");
				int n = io.nextInt();
				System.out.print("\n" + series.getValue(n));
				break;
			case 2:
				Series sum = series.getSum();
				if (sum == null) {
					System.out.println("Couldn't find sum series :(");
				}
				else {
					printWelcomePage(sum);
					openMenu(sum);
					printWelcomePage(series);
				}
				break;
			case 3:
				System.out.print("Enter initial location: ");
				int initialIndex = io.nextInt();
				System.out.print("Enter final location: ");
				int finalIndex = io.nextInt();
				series.printSeriesRange(initialIndex, finalIndex);
				break;			
			case 4:
				System.out.print("\n" + series.getANStringAlgebraicExpression());
				break;
			case 5:		
				AlgebraicExpression infinitSum = series.getInfinitSum();
				if (infinitSum == null) {
					System.out.println("The infinit sum is INFINITY!");
				}
				else {
					System.out.println(infinitSum);
				}
				break;
			case 6:
				UserInterface.main(null);
				break;
			case 7:
				return;
		}
		
		openMenu(series);
	}
}
