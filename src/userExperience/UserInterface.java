package userExperience;
import java.util.Scanner;

import seriesTypes.Series;
import seriesAnalysis.TrackSeries;

public class UserInterface {
	
	private final static Scanner io = new Scanner(System.in);
	
	private final static String BUG_MESSAGE = "\n*** You have found a bug ***\n"
			+ "I couldn't track your series or algebraic expression.\n"
			+ "Please send Peri a screenshot!";
	
	private static String[] getUserSeriesArgs(Scanner io) {
		
		System.out.print("Enter a series or an algebraic expression: ");
		
		String userLine = io.nextLine();
		String[] userSeriesStr = userLine.split(", ");
		if (userSeriesStr.length == 1) {
			userSeriesStr = userLine.split(",");
		}
		
		return userSeriesStr;
	}
	
	private static String[] getAndSetNextArg(String[] userSeries) {
		String[] newUserSeries = new String[userSeries.length + 1];
		
		System.out.print("\nWhat is the next arg? ");
		
		for (int i = 0; i < userSeries.length; i++) {
			System.out.print(userSeries[i] + ", ");
			newUserSeries[i] = userSeries[i];
		}
						
		String nextArg = io.next();
		
		newUserSeries[userSeries.length] = nextArg;
		userSeries = newUserSeries;
		return userSeries;
	}
	
	private static Series getUserSeries(String[] userSeriesArgs) {
		
		Series userSeries;
		if (userSeriesArgs.length < 4) {	
			userSeries = TrackSeries.trackSeries(userSeriesArgs, new String[] {"BaseSeries"});
			
			if (userSeries == null) {
				if (userSeriesArgs.length < 4) {
					userSeriesArgs = UserInterface.getAndSetNextArg(userSeriesArgs);				
				}
				userSeries = TrackSeries.trackSeries(userSeriesArgs);
			}
		}
		else {
			userSeries = TrackSeries.trackSeries(userSeriesArgs);
		}		
		
		return userSeries;
	}
	
	public static void throwBug(Exception e) throws Exception {
		throw(e);
		/*System.out.println(BUG_MESSAGE);
		io.nextLine();
		io.nextLine();*/
	}
	
	public static void main(String[] args) throws Exception {		
		
		String[] userSeriesStr = UserInterface.getUserSeriesArgs(io);
		Series userSeries = null;
		
		try {
			
			while (userSeriesStr.length < 3) {
				if (userSeriesStr.length == 1) { // AE Series
					userSeries = new Series(userSeriesStr[0]);
					break;				
				}
				System.out.println("\nYou have to enter 3 args or more!");
				userSeriesStr = UserInterface.getUserSeriesArgs(io);
			}
			
			if (userSeries == null) {			
				userSeries = UserInterface.getUserSeries(userSeriesStr);					
			}
			
			if (userSeries == null) {
				throwBug(null);
			}
			else {
				SeriesMenu.printWelcomePage(userSeries);
				SeriesMenu.openMenu(userSeries);				
			}			
			
		}
		catch (Exception e) {
			throwBug(e);
		}
		
	}

}
