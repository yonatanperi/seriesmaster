package seriesAnalysis;

import seriesTypes.*;

public class TrackSeries {
	
	private final static String[] SERIES_TYPES = {
			"ArithmeticSeries",
			"GeometricSeries",
			"PowerJump",
			"DifferenceSeries",
			"CyclicJumps",
			"Fibonacci",
			"DotSplit",
			"CyclicAppearance",
			"Polynomial"};
	private final static String[] BASE_SERIES_TYPES = { // 3 args are enough
			"ArithmeticSeries",
			"GeometricSeries",
			"PowerJump",
			"DotSplit",
			"CyclicAppearance"};
	
	public static Series trackSeries(String[] args, String[] seriesOptionalTypes) {		
		Series series;
		for (String seriesOptionalType : seriesOptionalTypes) {
			series = trackSeries(args, seriesOptionalType);
			if (series != null) {
				return series;
			}
		}
		return null;
	}
	
	public static Series trackSeriesExceptTypes(String[] args, String[] seriesAnOptionalTypes) {
		
		Series series;
		
		for (String seriesType : SERIES_TYPES) {
			if (!contains(seriesAnOptionalTypes, seriesType) &&
					!(contains(seriesAnOptionalTypes, "BaseSeries") &&
							contains(BASE_SERIES_TYPES, seriesType))) {
				series = trackSeries(args, seriesType);
				if (series != null) {
					return series;
				}
			}
		}
		return null;
	}
	
	public static Series trackSeries(String[] args) {
		return trackSeries(args, 0);
	}
	
	
	/**
	 * 
	 * @param seriesType is without spaces!
	 * @return the args series if succeeded, null if not.
	 */
	public static Series trackSeries(String[] args, String seriesType) {
		
		if (args.length < 3) {
			return null;
		}
		
		Series series = null;
		switch (seriesType) {
			case "BaseSeries":
				return trackSeries(args, BASE_SERIES_TYPES);
			case "ArithmeticSeries":
				series = new ArithmeticSeries(args);
				break;
			case "GeometricSeries":
				series = new GeometricSeries(args);
				break;
			case "PowerJump":
				series = new PowerJump(args);
				break;
			case "DotSplit":
				series = new DotSplit(args);
				break;
			case "CyclicAppearance":
				series = new CyclicAppearance(args);
				break;
		}
		if (args.length >= 4) {
			switch (seriesType) {
				case "DifferenceSeries":
					series = new DifferenceSeries(args);
					break;
				case "CyclicJumps":
					series = new CyclicJumps(args);
					break;				
				case "Fibonacci":
					series = new Fibonacci(args);
					break;				
				case "Polynomial":
					series = new Polynomial(args);
					break;
			}
		}
		
		if (series == null) {
			return null;
		}
		
		if (series.trackSeries()) {
			return series;
		}
		
		return null;
	}
	
	private static <E> boolean contains(E[] args, E checkedArg){
		for (E currentArg : args) {
			if (checkedArg == currentArg) {
				return true;
			}
		}
		return false;
	}
	
	private static Series trackSeries(String[] args, int seriesTypeIndex) {
		if (seriesTypeIndex == SERIES_TYPES.length) {
			return null;
		}
		Series series = trackSeries(args, SERIES_TYPES[seriesTypeIndex]);
		if (series == null) {
			return trackSeries(args, seriesTypeIndex + 1);
		}
		return series;
	}
	
}
